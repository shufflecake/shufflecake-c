/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "cli.h"
#include "commands.h"
#include "utils/sflc.h"
#include "utils/input.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/*
 * Change volume password
 *
 * @return Error code, 0 on success
 */
int sflc_cli_changePwd(char *block_device)
{	// Requires: block_device is a correct block device path
	sflc_cmd_OpenArgs open_args;
	sflc_cmd_ChangePwdArgs change_pwd_args;
	sflc_DmbCell dmb_cell;
	char old_pwd[SFLC_BIGBUFSIZE];
	size_t old_pwd_len;
	char new_pwd[SFLC_BIGBUFSIZE];
	size_t new_pwd_len;
	int err;

	open_args.bdev_path = block_device;
	
	/* Gather password */
	printf("Enter the password you want to change: ");
	err = sflc_safeReadPassphrase(old_pwd, SFLC_BIGBUFSIZE);
	if (err) {
		sflc_log_error("Could not read password; error %d", err);
		return err;
	}
	/* You can trust the length of strings input this way */
	old_pwd_len = strlen(old_pwd);
	/* Assign fields */
	open_args.pwd = old_pwd;
	open_args.pwd_len = old_pwd_len;

	/* Test the password */
	err = sflc_cmd_testPwd(&open_args, &dmb_cell);
	if (err) {
		sflc_log_error("Could not test password; error %d", err);
		return err;
	}

	/* Does this password open any volumes? */
	if (dmb_cell.vol_idx >= SFLC_DEV_MAX_VOLUMES) {
		printf("This password does not unlock any volume.\n");
		return 0;
	}

	/* Gather new password (no secure shell) */
	printf("This password opens volume number %lu .\n", dmb_cell.vol_idx);
	printf("Choose new password for volume %lu: ", dmb_cell.vol_idx);
	err = sflc_safeReadLine(new_pwd, SFLC_BIGBUFSIZE);
	if (err) {
		sflc_log_error("Could not read new password; error %d", err);
		return err;
	}
	/* You can trust the length of strings input this way */
	new_pwd_len = strlen(new_pwd);

	/* Assign fields */
	change_pwd_args.bdev_path = block_device;
	change_pwd_args.dmb_cell = &dmb_cell;
	change_pwd_args.new_pwd = new_pwd;
	change_pwd_args.new_pwd_len = new_pwd_len;

	/* Change password */
	err = sflc_cmd_changePwd(&change_pwd_args);
	if (err) {
		sflc_log_error("Could not change password; error %d", err);
		return err;
	}
	printf("Password changed successfully.\n");
	return err;
}
