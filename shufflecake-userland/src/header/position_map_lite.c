/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *
 *  This program is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the Free
 *  Software Foundation, either version 2 of the License, or (at your option)
 *  any later version. This program is distributed in the hope that it will be
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 *  Public License for more details. You should have received a copy of the
 *  GNU General Public License along with this program.
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>

#include "header.h"
#include "utils/sflc.h"
#include "utils/crypto.h"
#include "utils/math.h"
#include "utils/log.h"


/*****************************************************
 *          PUBLIC FUNCTIONS DEFINITIONS             *
 *****************************************************/

/* Create an encrypted empty position map for the given number of slices.
 * Allocates the memory (whole number of blocks) needed to host it.
 *
 * @param nr_slices The number of slices the device will be composed of.
 * @param volume_key The volume's data section encryption key, used to encrypt the
 *  position map as well.
 *
 * @return The memory buffer containing the position map */
void *sflite_epm_create(size_t nr_slices, size_t vol_idx, char *volume_key)
{
	char pmb[SFLC_BLOCK_SIZE];
	char iv[SFLC_AESXTS_IVLEN];
	void *epm;
	size_t nr_pmbs;
	uint64_t pblk_num;
	int err;

	// Each PosMapBlock holds up to 1024 PSIs (Physical Slice Index)
	nr_pmbs = ceil(nr_slices, SFLC_SLICE_IDX_PER_BLOCK);

	// Allocate EPM
	epm = malloc(nr_pmbs * SFLC_BLOCK_SIZE);
	if (!epm) {
		sflc_log_error("Could not malloc EPM");
		return NULL;
	}

	// Fill cleartext PMB with 0xFF
	memset(pmb, SFLC_EPM_FILLER, SFLC_BLOCK_SIZE);
	// First physical block number
	pblk_num = sflite_pmStartBlock(vol_idx, nr_slices);

	// Loop to encrypt each PMB
	int i;
	for (i = 0; i < nr_pmbs; i++) {
		// Set IV
		memset(iv, 0, SFLC_AESXTS_IVLEN);
		*((uint64_t*)iv) = htole64(pblk_num);

		// Encrypt
		err = sflc_aes256xts_encrypt(volume_key, pmb, SFLC_BLOCK_SIZE, iv,
				epm + i*SFLC_BLOCK_SIZE);
		if (err) {
			sflc_log_error("Could not encrypt %d-th block of PosMap; error %d", i , err);
			free(epm);
			return NULL;
		}

		// Increment block number
		pblk_num++;
	}

	return epm;
}
