# How to contribute to Shufflecake

Thank you for your interest in contributing! These guidelines are subject to change, so please check before submitting a contribution.

- [In a nutshell](#nutshell)
- [Asking support](#support)
- [Opening issues](#issues)
- [Code submission guidelines](#guidelines)
	- [Coding rules](#rules)
	- [Commits](#commits)
	- [Pull requests](#pr)
	- [Developer certificate of origin](#dco)



## In a nutshell <a id="user-content-nutshell"></a>

"We" are the maintainers of Shufflecake, please see the file README.md in this directory.

We do not accept anonymous code contributions: please use your full legal name when submitting, or anyway make it clearly visible in text. Opening issues anonymously is OK, though.

We require code contributors to sign a DCO, but no CLA or CTA. See [Developer certificate of origin](#dco) for instructions.

We are flexible in the way we accept contributions, although the best way is to submit an issue or a pull request through the project's git platform (e.g., Codeberg).

Always keep in mind that this project embraces the principles of privacy, libre software, and digital freedom.



## Asking support <a id="user-content-support"></a>

Before submitting a contribution, please make sure to check:

- Is it an issue? Check if it is a known problem before, i.e., if a similar open issues already exists.
- Is it a feature request? Consider whether it aligns with the goal of the project, or if an open issues about it already exists.
- Is it a code contribution? Please read below for submission guidelines.
- Did you find a security bug? Please follow the instructions in the SECURITY.md file in this directory.

In case of doubt, you are always welcome to reach out to our support channels (Jabber, Mastodon, etc) or by email.



## Opening issues <a id="user-content-issues"></a>

We are pretty relaxed in the git issues requirements, but please make sure to include all the necessary information. We will assign appropriate labels or edit the issue when necessary.



## Code submission guidelines <a id="user-content-guidelines"></a>

Please be verbose and accurate regarding what your contributions does and how, it will make our review process easier.

If you would like to implement a new breaking or major feature, please submit an issue with a proposal for your work first, to be sure that we can use it. Minor features can be crafted and submitted directly, but opening an issue beforehand is still recommended.



### Coding rules <a id="user-content-rules"></a>

We do not set a column limit to wrap code.

Use tabs to indent code, not spaces. You can use spaces if you need to fine-tune alignment for any reason. Submission using spaces for indentation instead of tabs will either be rejected or reformatted before being merged.

We are relatively flexible on C coding style, as long as it is reasonable. For example, a good reference is [this one](https://www.cs.umd.edu/~nelson/classes/resources/cstyleguide/).



### Commits <a id="user-content-commits"></a>

We (try to) follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/). In a nutshell:

```
<type>: <description>

[optional body]

[optional footer(s)]
```

The field `<type>` must be one of the following:

- build: Changes that affect the build system or external dependencies.
- docs: Documentation only changes.
- feat: A new feature or performance improvement.
- fix: A bug fix.
- refactor: A code change that neither fixes a bug nor adds a feature.
- revert: Reverts a previous commit. It must be followed by the header (title) of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.
- other: Anything else (please use this sparingly).

A breaking change introduces a potential incompatibility with the current version of Shufflecake, for example it might make impossible to open a Shufflecake volume created with the previous version. Breaking changes must be marked with an exclamation mark following the `<type>` field. Example: `feat!: Change hash algorithm from MD5 to SHA3`.

Ideally, we would like the `<description>` to have the first letter capitalized, be separated by a whitespace from the preceding column `:`, and to __not__ end with a period `.`. Use imperative, e.g.: `fix: Remove bug in file.c` instead of `fix: Removed bug in file.c`.

You can use `<footer>` to reference an existing issue, e.g.: `Fixes #1234`.



### Pull requests <a id="user-content-pr"></a>

Every pull request (PR) should ideally target one change at a time. For example, it is best to avoid a PR which fixes a bug __and__ introduces a new feature at the same time. Split in two PRs instead.

When creating a PR, please always create an ad-hoc branch for that. Do __not__ try to merge into `main`. Choose a descriptive name for the new branch. Good examples:

 - `fix/memleak`
 - `add_color_msg`
 - `misc/docs/userguidev2`

Bad examples:

 - `roys_fix`
 - `420crypt`
 - `enable_new_header_field_and_fix_issue_from_jan_25`



### Developer certificate of origin <a id="user-content-dco"></a>

Many small open source projects do not really care about copyright assignment, but given the nature of the Shufflecake Project we want to avoid ambiguous legal situations. Shufflecake is, and will always be, fully FOSS/Libre. Distributing copyright across all the contributors helps to avert a scenario where a few people, even us (the maintainers) could collude at any point in the future to arbitraily change the software license to a commercial or non-free one. For this reason, the Shufflecake Project will never ask contributors to sign a Contributor License Agreement (CLA) or a Copyright Transfer Agreement (CTA). However, since in many cases contributors do not have full freedom in retaining the copyright of the code they contribute to, we ask contributors to sign off a Developer Certificate of Origin (DCO), the text of which you can find [here](https://developercertificate.org/), but we also copy it here below for convenience:

```
Developer Certificate of Origin
Version 1.1

Copyright (C) 2004, 2006 The Linux Foundation and its contributors.

Everyone is permitted to copy and distribute verbatim copies of this
license document, but changing it is not allowed.


Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

In practice:

 1. Make sure you have a PGP key to sign off your contributions.
 	1. The key must be minimum 2048 bit RSA or equivalent/above security level.
 	2. It must contain an identity with your full legal name.
 	3. It must be publicly available (i.e., upload it on a public keyserver/keyoxide/nostr etc).
 	3. If it's not your main, i.e., publicly known PGP key, make sure it is signed by it.
 2. Configure the key to be used on our git platform (e.g., Codeberg).
 	1. Import the public part of the key in your profile (e.g.: Settings -> SSH/OpenPGP Keys).
 	2. Use the key to sign your commits.
 	3. Use the `-s` flag when committing your contribution to Shufflecake.
 3. If you submit your contribution differently, you can send us a PGP-signed email where you confirm your acceptance of the DCO in regard to your contribution.
 	1. Include a mention of your contribution, e.g. a description in text, or commit hash).
 4. Once your contribution has been reviewed and merged, your name will be added to the list of contributors to the `AUTHORS` file in this directory.


