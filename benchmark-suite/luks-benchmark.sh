#!/bin/bash

#  Copyright The Shufflecake Project Authors (2022)
#  Copyright The Shufflecake Project Contributors (2022)
#  Copyright Contributors to the The Shufflecake Project.
  
#  See the AUTHORS file at the top-level directory of this distribution and at
#  <https://www.shufflecake.net/permalinks/shufflecake-c/AUTHORS>
  
#  This file is part of the program shufflecake-c, which is part of the Shufflecake 
#  Project. Shufflecake is a plausible deniability (hidden storage) layer for 
#  Linux. See <https://www.shufflecake.net>.
  
#  This program is free software: you can redistribute it and/or modify it 
#  under the terms of the GNU General Public License as published by the Free 
#  Software Foundation, either version 2 of the License, or (at your option) 
#  any later version. This program is distributed in the hope that it will be 
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
#  Public License for more details. You should have received a copy of the 
#  GNU General Public License along with this program. 
#  If not, see <https://www.gnu.org/licenses/>.

# Benchmarking script for LUKS/dm-crypt

# Variables
SCRIPTNAME=$(basename "$0")
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
LOOP_FILENAME="$SCRIPT_DIR/luks-benchmark-loop-file.img"
LOOP_DEVICE=""
TIMEFORMAT='%3R'

# Colors
BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No color

# Help
print_help() {
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo -e "${BLUE}Usage: ${SCRIPTNAME} [OPTION]... [BLOCKDEVICE]${NC}"
    echo " "
    echo "This script is used to benchmark LUKS/dm-crypt on this machine."
    echo "This script is part of the Shufflecake benchmark suite."
    echo "Shufflecake is a plausible deniability (hidden storage) layer for Linux."
    echo -e "Please visit ${BLUE}https://www.shufflecake.net${NC} for more info and documentation."
    echo " "
    echo "This script requires root because it operates on block devices, please run it "
    echo -e "with ${BLUE}sudo${NC}. It does the following:"
    echo "1) Creates a LUKS volume within a given device."
    echo "2) Opens the volume, formats it with ext4 and mounts it."
    echo "3) Performs various fio r/w stress operations on it."
    echo "4) Unmounts and closes the used volume."
    echo " "
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo "You can pass the path to a block device as an optional argument, otherwise the "
    echo "script will ask for one. If no path is provided, the script will create a 1 GiB"
    echo "local file and use it to back a loop device as a virtual block device to be "
    echo "formatted with the appropriate tools. The file will be removed at the end."
    echo " "
#         xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
    echo -e "${BLUE}WARNING: ALL CONTENT OF THE PROVIDED BLOCK DEVICE WILL BE ERASED!${NC}"
    echo " "
    exit 0
}


# Function for debugging
bpx() {
    if [ -z "$1" ]; then
        echo "BPX: Paused. Press any key to continue..." >&2
    else
        echo "BPX: $1. Press any key to continue..." >&2
    fi
    read -n1 -s
}

# Show usage
usage() {
    echo -e "Use ${BLUE}${SCRIPTNAME} --help${NC} for usage and help."
}

# Check that this is run as root
check_sudo() {
    if [[ $EUID -ne 0 ]]; then
        echo -e "${RED}Error: This script must be run as root.${NC}"
        usage
        exit 1
    fi
}

# Function to check if argument is a block device
check_block_device() {
    if [ -b "$1" ]; then
        echo "OK, block device path $1 is valid." >&2
    else
        echo -e "${RED}Error: $1 is not a valid block device, aborting.${NC}"
        usage
        exit 1
    fi
}

# Function to create loop device
create_loop_device() {
    echo "I will now try to create a file $LOOP_FILENAME ..." >&2
    if [ -e "$LOOP_FILENAME" ]; then
        echo -e "${RED}Error: Impossible to generate file, $LOOP_FILENAME already exists.${NC}"
        exit 1
    fi
    sudo dd if=/dev/zero of="$LOOP_FILENAME" bs=1M count=1024 > /dev/null
    echo "Writing of empty file complete. I will now try to attach it to a new loop device..." >&2
    LOOP_DEVICE=$(sudo losetup -f --show "$LOOP_FILENAME")
    echo "Successfully created loop device $LOOP_DEVICE ." >&2
    echo "$LOOP_DEVICE"
}

# Generate a suitable LUKS volume name
find_luksvolname() {
    volname="/dev/mapper/luks-benchmark-testvol" #placeholder
    echo "$volname"
}

# Function for user confirmation
confirm() {
    while true; do
        echo -e "${BLUE}Are you sure you want to proceed? All data on disk $BLOCK_DEVICE will be erased. (y/n)${NC}"
        read -r response
        case "$response" in
            [yY]|[yY][eE][sS])  # Responded Yes
                return 0        # Return 0 for Yes (success, convention for bash scripting)
                ;;
            [nN]|[nN][oO])  # Responded No
                return 1    # Return 1 for No (error, convention for bash scripting)
                ;;
            *)  # Responded something else
                echo "Please press only (y)es or (n)o."
                ;;
        esac
    done
}

# Benchmarks
benchmark() {

    LUKSVOLUME="luks-test"
    MNTPOINT=""
    PASSPHRASE="mypassword"
    TESTNAME="luks"
    RUNTIME="20" # running time in seconds FOR EACH TEST
    DATASIZE="500M"
    TESTFILENAME="testfile"
    echo "Starting benchmark for LUKS/dm-crypt..."
    echo "Initializing block device $BLOCK_DEVICE as a LUKS volume..."
    etime=$( (time echo -n "$PASSPHRASE" | cryptsetup --batch-mode --cipher aes-xts-plain64 luksFormat $BLOCK_DEVICE - > /dev/null) 2>&1 )
    echo -e "${GREEN}Action luksFormat took $etime seconds.${NC}"
    echo "LUKS device initialized. Opening encrypted volume..."
    etime=$( (time echo -n "$PASSPHRASE" | cryptsetup luksOpen $BLOCK_DEVICE $LUKSVOLUME - > /dev/null) 2>&1 )
    echo -e "${GREEN}Action luksOpen took $etime seconds.${NC}"
    echo "LUKS volume opened as /dev/mapper/$LUKSVOLUME. Formatting with ext4..."
    # format with ext4, but mute output (too verbose)
    mkfs.ext4 /dev/mapper/$LUKSVOLUME > /dev/null
    echo "Volume /dev/mapper/$SFLCVOLUME formatted. Mounting that..."
    # assign and create MNTPOINT
    MNTPOINT=$(realpath "./luks_mnt")
    mkdir $MNTPOINT
    # mount
    mount /dev/mapper/$LUKSVOLUME $MNTPOINT
    echo "Volume mounted at $MNTPOINT. Starting fio tests..."
    # TESTS HERE
    # test 01: random read
    echo "Test 01: random read with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-r-rnd --ioengine=libaio --iodepth=32 --rw=randread --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, read_iops: .read.iops, read_bw: .read.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 02: random write
    echo "Test 02: random write with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-w-rnd --ioengine=libaio --iodepth=32 --rw=randwrite --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, write_iops: .write.iops, write_bw: .write.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 03: sequential read
    echo "Test 03: sequential read with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-r-seq --ioengine=libaio --iodepth=32 --rw=read --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, read_iops: .read.iops, read_bw: .read.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # test 04: sequential write
    echo "Test 04: sequential write with a queue of 32 4kiB blocks on a file (${RUNTIME}s)..."
    OUTPUT=$(fio --name=$TESTNAME-w-seq --ioengine=libaio --iodepth=32 --rw=write --bs=4k --numjobs=1 --direct=1 --size=$DATASIZE --runtime=$RUNTIME --time_based --end_fsync=1 --filename=$MNTPOINT/$TESTFILENAME --output-format=json | jq '.jobs[] | {name: .jobname, write_iops: .write.iops, write_bw: .write.bw}')
    echo -e "${GREEN}${OUTPUT}${NC}"
    # END TESTS
    echo "LUKS/dm-crypt fio tests ended. Unmounting volume."
    # unmount
    umount $MNTPOINT
    rmdir $MNTPOINT
    echo "Volume unmounted. Closing LUKS device..."
    # close
    etime=$( (time cryptsetup luksClose $LUKSVOLUME > /dev/null) 2>&1 )
    echo -e "${GREEN}Action close took $etime seconds.${NC}"
    #end    
}

# Clean up
cleanup() {
    echo "Exiting and cleaning..."
    if [[ -n $LOOP_DEVICE ]]; then
        echo "Detaching $LOOP_DEVICE ..."
        sudo losetup -d "$LOOP_DEVICE"
        echo "Deleting $LOOP_FILENAME ..."
        sudo rm -f "$LOOP_FILENAME"
        echo "Loop device detached and backing file deleted."
    fi
}


#####################################################################

# MAIN SCRIPT BODY STARTS HERE

#####################################################################

# BANNER
#                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars
echo -e "${BLUE}===============================================================================${NC}"
echo -e "${BLUE}                    Benchmark Suite Script for LUKS/dm-crypt${NC}"
echo -e "${BLUE}===============================================================================${NC}"


# PRELIMINARY: PARSE HELP, SUDO, AND CHECK CRYPTSETUP EXISTS

case "$1" in
	# help
    --help|-?)
        print_help
        exit 0
        ;;
esac

check_sudo

if ! which cryptsetup >/dev/null; then
    echo -e "${RED}ERROR: cryptsetup not found, please install it.${NC}"
    exit 1
fi

echo " "


# PARSER

case "$1" in
    "")
#             xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   79 chars

        echo "Now you will be asked to enter the path for a block device to be used for the "
        echo "benchmarks (all content will be erased). If no path is provided (default" 
        echo "choice), then the script will create a 1 GiB file in the current directory and "
        echo "use it to back a loop device instead, then the file will be removed at the end."
        echo " "
        echo -n "Please enter the path for a block device (default: none): "
        read BLOCK_DEVICE
        if [ -z "$BLOCK_DEVICE" ]; then
            echo "No path provided, creating a local file and loop device..."
            LOOP_DEVICE=$(create_loop_device)
            BLOCK_DEVICE=$LOOP_DEVICE
        fi
        
        ;;
        
        # argument passed
    *)
    	BLOCK_DEVICE="$1"
        ;;
esac

check_block_device "$BLOCK_DEVICE"

# MAIN PROGRAM

if confirm; then
    benchmark
else
    echo "Aborting..."
fi

cleanup




