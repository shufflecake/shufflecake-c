
# Shufflecake - Benchmark Suite

This is a suite of scripts used to benchmark Shufflecake and other disk encryption solutions on the local machine. Shufflecake is a plausible deniability (hidden storage) layer for Linux. Please visit <https://www.shufflecake.net> for more info and documentation.

For each of these scripts you must first install and configure the related tool first (`cryptsetup`, `veracrypt`, etc).
 
Most of these script require root because they operates on block devices, please run them with `sudo`. You can invoke them with `-?` or `--help` for detailed usage instructions of each one of them.

