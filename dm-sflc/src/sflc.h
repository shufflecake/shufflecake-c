/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SFLC_H
#define _SFLC_H


#include <linux/device-mapper.h>

#include "sflc_types.h"
#include "sflc_constants.h"


/*
 *----------------------------
 * Global variables
 *----------------------------
 */

/* Every mode provides a full set of algorithms */
extern struct sflc_mode_ops *sflc_all_mode_ops[SFLC_NR_MODES];

/* Array of all open devices */
extern struct mutex sflc_alldevs_lock;
extern struct sflc_device_base **sflc_alldevs;
/* Next free device id */
extern u32 sflc_free_devid;	/* The lowest free devID */


/*
 *----------------------------
 * Functions
 *----------------------------
 */

/* Device base */
int sflc_dev_base_init(struct sflc_device_base *sd_base, struct dm_target *ti,
		       int argc, char **argv);
void sflc_dev_base_exit(struct sflc_device_base *sd_base);

/* Volume base */
int sflc_vol_base_init(struct sflc_volume_base *sv_base,
		       struct sflc_device_base *sd_base, struct dm_target *ti,
		       int argc, char **argv);
void sflc_vol_base_exit(struct sflc_volume_base *sv_base);

/* Sysfs */
int sflc_sysfs_init(void);
void sflc_sysfs_exit(void);
int sflc_sysfs_register_device_base(struct sflc_device_base *sd_base);
void sflc_sysfs_unregister_device_base(struct sflc_device_base *sd_base);
int sflc_sysfs_register_volume_base(struct sflc_volume_base *sv_base);
void sflc_sysfs_unregister_volume_base(struct sflc_volume_base *sv_base);


#endif	/* _SFLC_H */
