/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sflc.h"

#include <linux/delay.h>

/*
 *----------------------------
 * Top-level entries
 *----------------------------
 */

static ssize_t next_dev_id_show(struct module_attribute *mattr, struct module_kobject *mkobj, char *buf)
{
	ssize_t ret;

	if (mutex_lock_interruptible(&sflc_alldevs_lock))
		return -ERESTARTSYS;
	ret = sysfs_emit(buf, "%u\n", sflc_free_devid);
	mutex_unlock(&sflc_alldevs_lock);

	return ret;
}

static struct kset *bdevs_kset;
static struct module_attribute devid_mattr = __ATTR_RO(next_dev_id);

int sflc_sysfs_init()
{
	int err;

	bdevs_kset = kset_create_and_add(SFLC_SYSFS_BDEVS, NULL, &THIS_MODULE->mkobj.kobj);
	if (!bdevs_kset) {
		err = -ENOMEM;
		DMERR("Could not create %s kset", SFLC_SYSFS_BDEVS);
		goto bad_bdevs;
	}

	err = sysfs_create_file(&THIS_MODULE->mkobj.kobj, &devid_mattr.attr);
	if (err) {
		DMERR("Could not create %s file", SFLC_SYSFS_DEVID);
		goto bad_devid;
	}

	return 0;


bad_devid:
	kset_unregister(bdevs_kset);
bad_bdevs:
	return err;
}

void sflc_sysfs_exit()
{
	sysfs_remove_file(&THIS_MODULE->mkobj.kobj, &devid_mattr.attr);
	kset_unregister(bdevs_kset);
}


/*
 *----------------------------
 * Device entries
 *----------------------------
 */

static ssize_t dev_id_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);

	return sysfs_emit(buf, "%u\n", sd_base->dev_id);
}

static ssize_t volumes_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);

	return sysfs_emit(buf, "%u\n", sd_base->nr_volumes);
}


static struct kobj_attribute dev_id_kattr = __ATTR_RO(dev_id);
static struct kobj_attribute volumes_kattr = __ATTR_RO(volumes);
static struct attribute *sflc_device_default_attrs[] = {
	&dev_id_kattr.attr,
	&volumes_kattr.attr,
	NULL
};
ATTRIBUTE_GROUPS(sflc_device_default);

static void sflc_device_base_kobj_release(struct kobject *kobj)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);
	complete(&sd_base->kobj_released);
}

static struct kobj_type sflc_device_base_ktype = {
	.release = sflc_device_base_kobj_release,
	.sysfs_ops = &kobj_sysfs_ops,
	.default_groups = sflc_device_default_groups
};

int sflc_sysfs_register_device_base(struct sflc_device_base *sd_base)
{
	int err;

	/* Completion */
	init_completion(&sd_base->kobj_released);

	/* Register directory <MAJOR>:<MINOR>/ under bdevs/ */
	sd_base->kobj.kset = bdevs_kset;
	err = kobject_init_and_add(&sd_base->kobj, &sflc_device_base_ktype, NULL,
				   "%s", sd_base->name);
	if (err)
		goto bad;
	/* Emit uevent */
	kobject_uevent(&sd_base->kobj, KOBJ_ADD);

	return 0;


bad:
	kobject_put(&sd_base->kobj);
	wait_for_completion(&sd_base->kobj_released);
	return err;
}

void sflc_sysfs_unregister_device_base(struct sflc_device_base *sd_base)
{
	kobject_put(&sd_base->kobj);
	wait_for_completion(&sd_base->kobj_released);
}


/*
 *----------------------------
 * Volume entries
 *----------------------------
 */

static void sflc_volume_base_kobj_release(struct kobject *kobj)
{
	struct sflc_volume_base *sv_base = container_of(kobj, struct sflc_volume_base, kobj);

	complete(&sv_base->kobj_released);
}

static struct kobj_type sflc_volume_base_ktype = {
	.release = sflc_volume_base_kobj_release,
	.sysfs_ops = &kobj_sysfs_ops,
};

int sflc_sysfs_register_volume_base(struct sflc_volume_base *sv_base)
{
	int err;

	/* Completion */
	init_completion(&sv_base->kobj_released);

	/* Register directory <svol->name>/ under device directory */
	err = kobject_init_and_add(&sv_base->kobj, &sflc_volume_base_ktype,
				   &sv_base->sd_base->kobj,
				   "%s", sv_base->name);
	if (err)
		goto bad;

	/* Emit uevent */
	kobject_uevent(&sv_base->kobj, KOBJ_ADD);

	return 0;


bad:
	kobject_put(&sv_base->kobj);
	wait_for_completion(&sv_base->kobj_released);
	return err;
}

void sflc_sysfs_unregister_volume_base(struct sflc_volume_base *sv_base)
{
	kobject_put(&sv_base->kobj);
	wait_for_completion(&sv_base->kobj_released);
}

