/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * This kernel module implements Shufflecake as an in-kernel device driver,
 * built on top of the device-mapper framework.
 * Shufflecake supports several "modes" (currently Legacy and Lite). Each mode
 * is a distinct set of algorithms which implements a specific flavour of the
 * Shufflecake scheme; they are very different and largely incompatible with
 * each other. For this reason, each mode has its own independent, complete
 * implementation - located in a subdirectory of the root - and exposes a
 * standardised interface to the top-level "dispatcher", which only takes care
 * of the few bookkeeping tasks which are common to all modes, and displays a
 * unified view to user space through the sysfs.
 *
 * In order to achieve an intuitive and maintainable separation between the
 * dispatcher and the modes, a "lightweight" OOP-style coding convention is
 * followed: not fully general, but just enough to allow for one-level class
 * inheritance and type-checked method polymorphism.
 * Base "classes" exist to represent devices and volumes (see Shufflecake docs
 * to learn what these terms mean): they are structs containing the fields
 * needed by the dispatcher for its bookkeeping. Derived "classes" exist to
 * represent mode-specific devices and volumes: they are structs embedding a
 * base-class struct "by-value", plus their own specific fields.
 * Type-checked polymorphism is achieved by having device and volume methods
 * take as argument a pointer to a base-class struct, instead of an opaque void
 * pointer. The pointer is then "down-cast" in the mode-specific method.
 */

#ifndef _SFLC_TYPES_H
#define _SFLC_TYPES_H


#include <linux/device-mapper.h>

#include "sflc_constants.h"


/*
 *----------------------------
 * Structs
 *----------------------------
 */

/**
 * The virtual table.
 * Contains all the concrete method pointers provided by a Shufflecake mode
 * (devices + volumes + devmapper).
 * It is a field in the base class, but will be set by the constructor of the
 * derived class.
 */
struct sflc_mode_ops;

/**
 * Base class for devices.
 * It is inherited by derived classes for mode-specific devices.
 */
struct sflc_device_base
{
	/* Shufflecake-unique device ID */
	u32			dev_id;
	/* <MAJOR>:<MINOR> */
	char			name[16];
	/* Shufflecake mode: legacy/lite */
	u32			mode;
	/* Number of (open) volumes */
	u32			nr_volumes;

	/* Sysfs */
	struct kobject		kobj;
	struct completion	kobj_released;

	/* Virtual table */
	struct sflc_mode_ops	*ops;
};

/**
 * Base class for volumes.
 * It is inherited by derived classes for mode-specific volumes.
 */
struct sflc_volume_base
{
	/* Backing device */
	struct sflc_device_base	*sd_base;

	/* Volume index within the device */
	u32			vol_idx;
	/* Volume name: sflc_<devID>_<volIdx> */
	char			name[32];

	/* Sysfs */
	struct kobject		kobj;
	struct completion	kobj_released;

	/* Virtual table */
	struct sflc_mode_ops	*ops;
};


/*
 *----------------------------
 * Methods
 *----------------------------
 */

/* Device constructor */
typedef struct sflc_device_base* (*sflc_mode_dev_ctr_fn) (
		struct dm_target *ti, int argc, char **argv);
/* Device destructor */
typedef void (*sflc_mode_dev_dtr_fn) (struct sflc_device_base* sd_base);

/* Volume constructor */
typedef struct sflc_volume_base* (*sflc_mode_vol_ctr_fn) (
		struct sflc_device_base *sd_base, struct dm_target *ti,
		int argc, char **argv);
/* Volume destructor */
typedef void (*sflc_mode_vol_dtr_fn) (struct sflc_volume_base* sv_base);

/**
 * A Shufflecake mode must provide ctr() and dtr() for devices and volumes,
 * as well as devmapper-related methods.
 */
struct sflc_mode_ops
{
	sflc_mode_dev_ctr_fn	dev_ctr;
	sflc_mode_dev_dtr_fn	dev_dtr;
	sflc_mode_vol_ctr_fn	vol_ctr;
	sflc_mode_vol_dtr_fn	vol_dtr;
	dm_map_fn		map;
	dm_io_hints_fn		io_hints;
	dm_iterate_devices_fn	iterate_devices;
};


#endif	/* _SFLC_TYPES_H */
