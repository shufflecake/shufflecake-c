/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/delay.h>

#include "sflc.h"
#include "legacy/sflc_legacy.h"


/* Init a sflc_device_base */
int sflc_dev_base_init(struct sflc_device_base *sd_base, struct dm_target *ti,
		       int argc, char **argv)
{
	u32 dev_id;
	dev_t devt;
	int err;

	/* Parse arguments */
	sscanf(argv[1], "%u", &dev_id);
	err = lookup_bdev(argv[2], &devt);
	if (err)
		return err;

	/* Assign fields */
	sd_base->dev_id = dev_id;
	sd_base->nr_volumes = 0;
	format_dev_t(sd_base->name, devt);
	/* Fields `mode` and `ops` will be set by the derived constructor */

	/* Register with sysfs */
	err = sflc_sysfs_register_device_base(sd_base);
	if (err)
		return err;

	return 0;
}


void sflc_dev_base_exit(struct sflc_device_base *sd_base)
{
	sflc_sysfs_unregister_device_base(sd_base);
}

/* Init a sflc_volume_base */
int sflc_vol_base_init(struct sflc_volume_base *sv_base,
		       struct sflc_device_base *sd_base, struct dm_target *ti,
		       int argc, char **argv)
{
	u32 vol_idx;
	int err;

	/* Parse arguments */
	sscanf(argv[3], "%u", &vol_idx);

	/* Assign fields */
	sv_base->vol_idx = vol_idx;
	sprintf(sv_base->name, "sflc_%u_%u", sd_base->dev_id, vol_idx);
	sv_base->sd_base = sd_base;
	/* Field `ops` will be set by the derived constructor */

	/* Register with sysfs */
	err = sflc_sysfs_register_volume_base(sv_base);
	if (err)
		return err;

	return 0;
}

void sflc_vol_base_exit(struct sflc_volume_base *sv_base)
{
	sflc_sysfs_unregister_volume_base(sv_base);
}

