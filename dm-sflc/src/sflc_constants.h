/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/* Here we gather Shufflecake-wide constants, independent of the mode (legacy/lite) */

#ifndef _SFLC_CONSTANTS_H_
#define _SFLC_CONSTANTS_H_


#define SFLC_TARGET_NAME	"shufflecake"
#define DM_MSG_PREFIX 		"sflc"


#define SFLC_VER_MAJOR 0
#define SFLC_VER_MINOR 5
#define SFLC_VER_REVISION 2
#define SFLC_VER_SPECIAL ""

#define STRINGIFY0(s) # s
#define STRINGIFY(s) STRINGIFY0(s)

#define SFLC_VERSION STRINGIFY(SFLC_VER_MAJOR)"."STRINGIFY(SFLC_VER_MINOR)"."STRINGIFY(SFLC_VER_REVISION)""SFLC_VER_SPECIAL


/* Each device can be formatted and used with a mode of choice, irrespective of the other devices */
#define SFLC_MODE_LEGACY 0
#define SFLC_MODE_LITE 1
#define SFLC_NR_MODES 2


/* Max number of volumes per device */
#define SFLC_DEV_MAX_VOLUMES	15
/* Max number of open devices at any given time */
#define SFLC_MAX_DEVS		1024


/* Sysfs entries under /sys/module/dm_sflc/ */
#define SFLC_SYSFS_BDEVS	"bdevs"
#define SFLC_SYSFS_DEVID	"next_dev_id"


#endif	/* _SFLC_CONSTANTS_H_ */
