/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sflc_lite.h"
#include <linux/delay.h>


static void sflite_write_endio(struct bio *phys_bio);


void sflite_write_work_fn(struct work_struct *work)
{
	struct sflite_io *sio = container_of(work, struct sflite_io, work);
	struct sflite_volume *svol = sio->svol;
	struct sflite_device *sdev = svol->sdev;
	struct bio *orig_bio = sio->orig_bio;
	struct bio_vec bvl = bio_iovec(orig_bio);
	struct bio *phys_bio;
	struct page *page;
	u32 lsi = sio->lsi;
	u32 block_offset = sio->block_offset;
	u32 psi;
	int err;

//	DMWARN("WRITE: dequeued. Sector = %llu", orig_bio->bi_iter.bi_sector);
//	msleep(100);


	/* Read existing mapping, or create new one */
	if (mutex_lock_interruptible(&svol->posmap_lock)) {
		orig_bio->bi_status = BLK_STS_IOERR;
		goto endio;
	}
	psi = svol->posmap[lsi];
	/* If LSI unmapped, create new mapping, while holding the lock */
	if (psi == SFLITE_PSI_INVALID) {
//		DMWARN("WRITE: unmapped LSI %u, sampling PSI", lsi);
//		msleep(100);

		err = sflite_create_persistent_slice_mapping(svol, lsi, &psi);
		if (err){
			DMERR("Could not create slice mapping; error %d", err);
			mutex_unlock(&svol->posmap_lock);
			orig_bio->bi_status = BLK_STS_IOERR;
			goto endio;
		}
//		DMWARN("WRITE: sampled PSI %u for LSI %u", psi, lsi);
//		msleep(100);
	}
	mutex_unlock(&svol->posmap_lock);
	sio->psi = psi;

	/* Allocate physical bio */
	phys_bio = bio_alloc_bioset(svol->dm_dev->bdev, 1, orig_bio->bi_opf,
		GFP_NOIO, &sdev->bioset);
	if (!phys_bio) {
		DMERR("Could not allocate physical bio");
		orig_bio->bi_status = BLK_STS_IOERR;
		goto endio;
	}
        /* Insert in the I/O struct */
        sio->phys_bio = phys_bio;

        /* Physical bio needs its own page */
	page = alloc_pages(GFP_NOIO, 0);
	if (!page) {
		DMERR("Could not allocate page for physical bio");
		orig_bio->bi_status = BLK_STS_IOERR;
		goto bad_alloc_page;
	}

	/* Remap sector */
        phys_bio->bi_iter.bi_sector = SFLITE_PHYS_BIO_SECTOR(sdev, psi, block_offset);
	/* Encrypt */
	err = sflite_crypt_block_page(svol->tfm, bvl.bv_page, page,
			phys_bio->bi_iter.bi_sector >> SFLITE_BLOCK_SHIFT, WRITE);
	if (err) {
		DMERR("Could not encrypt bio; error %d", err);
		orig_bio->bi_status = BLK_STS_IOERR;
		goto bad_encrypt;
	}

	/* Add page to bio */
	__bio_add_page(phys_bio, page, SFLITE_BLOCK_SIZE, 0);
	/* Set fields for the endio */
        phys_bio->bi_private = sio;
        phys_bio->bi_end_io = sflite_write_endio;
        /* Submit */
        dm_submit_bio_remap(orig_bio, phys_bio);

	return;


bad_encrypt:
	__free_page(page);
bad_alloc_page:
	bio_put(phys_bio);
endio:
	bio_endio(orig_bio);
	return;
}

static void sflite_write_endio(struct bio *phys_bio)
{
	struct sflite_io *sio = phys_bio->bi_private;
	struct bio *orig_bio = sio->orig_bio;

	/* If physical bio failed, then fail-fast */
	if (phys_bio->bi_status != BLK_STS_OK) {
		orig_bio->bi_status = phys_bio->bi_status;
		DMWARN("WRITE ENDIO: phys_bio failed");
		goto endio;
	}

	/* Advance original bio by one block */
	bio_advance(orig_bio, SFLITE_BLOCK_SIZE);
	orig_bio->bi_status = BLK_STS_OK;

endio:
	/* Free the physical bio and its page */
	bio_free_pages(phys_bio);
	bio_put(phys_bio);
	/* End original bio */
        bio_endio(orig_bio);

        return;
}

