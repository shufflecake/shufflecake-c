/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/minmax.h>
#include <linux/delay.h>
#include "dm_io_helper.h"
#include "sflc_lite.h"


/* Helpers */

#define IS_PSI_TAKEN(sdev, psi)	( (sdev)->slices_ofld[(psi)] )
#define NEXT_RANDOM_PSI(sdev)	( (sdev)->prmslices[(sdev)->prmslices_octr] )
#define IS_LAST_LSI_IN_BLOCK(lsi, sdev) ( (((lsi)+1) % SFLITE_PSIS_PER_BLOCK == 0) ||	\
		(((lsi)+1) == (sdev)->tot_slices) )


/*
 *----------------------------
 * Create slice mapping
 *----------------------------
 */

/**
 * Return the next free PSI in the device's shuffled array, without modifying
 * the device state.
 *
 * MUTEX: @sdev->slices_lock must be held.
 */
static int peek_next_free_psi(struct sflite_device *sdev, u32 *psi)
{
	if (unlikely(!sdev->nr_free_slices))
		return -ENOSPC;
	if (unlikely(sdev->prmslices_octr >= sdev->tot_slices)) {
		DMCRIT("octr = %u, tot_slices = %u, free_slices = %u", sdev->prmslices_octr, sdev->tot_slices, sdev->nr_free_slices);
		print_hex_dump(KERN_CRIT, "prmslices(REV) ", DUMP_PREFIX_OFFSET, 32, 4, sdev->prmslices, 4*sdev->tot_slices, false);
		msleep(10000);
		print_hex_dump(KERN_CRIT, "ofld(REV)      ", DUMP_PREFIX_OFFSET, 32, 1, sdev->slices_ofld, sdev->tot_slices, false);
		msleep(10000);
		return -ENOTRECOVERABLE; // Grave inconsistency
	}

	/* Invariant: @prmslices_octr points to a free slice */
	*psi = NEXT_RANDOM_PSI(sdev);
	if (unlikely(IS_PSI_TAKEN(sdev, *psi))){
		DMCRIT("octr = %u, tot_slices = %u, free_slices = %u", sdev->prmslices_octr, sdev->tot_slices, sdev->nr_free_slices);
		DMCRIT("PSI %u is occupied", *psi);
		print_hex_dump(KERN_CRIT, "prmslices ", DUMP_PREFIX_OFFSET, 32, 4, sdev->prmslices, 4*sdev->tot_slices, false);
		msleep(10000);
		print_hex_dump(KERN_CRIT, "ofld      ", DUMP_PREFIX_OFFSET, 32, 1, sdev->slices_ofld, sdev->tot_slices, false);
		msleep(10000);
		return -ENOTRECOVERABLE; // Grave inconsistency
	}

	return 0;
}

/**
 * Map LSI => PSI, only in memory.
 * Sanity checks to be performed by the caller.
 *
 * MUTEX: @sdev->slices_lock must be held.
 * MUTEX: @svol->posmap_lock must be held, except under volume ctor.
 */
static void _create_local_slice_mapping(struct sflite_volume *svol, u32 lsi, u32 psi)
{
	struct sflite_device *sdev = svol->sdev;

	/* Grab it from the device */
	sdev->slices_ofld[psi] = true;
	sdev->nr_free_slices--;
	// Preserve the invariant: @prmslices_octr must point to a free slice
	while(sdev->prmslices_octr < sdev->tot_slices &&
	    IS_PSI_TAKEN(sdev, NEXT_RANDOM_PSI(sdev))) {
		sdev->prmslices_octr++;
	}

	/* Insert in the volume */
	svol->posmap[lsi] = psi;
	svol->nr_mapped_slices++;

	return;
}

/**
 * Delete mapping for the given LSI, only in memory.
 * Sanity checks to be performed by the caller.
 *
 * MUTEX: @svol->posmap_lock must be held, except under volume ctor.
 */
static void _delete_local_slice_mapping(struct sflite_volume *svol, u32 lsi)
{
	/* Delete mapping in the volume */
	svol->posmap[lsi] = SFLITE_PSI_INVALID;
	svol->nr_mapped_slices--;

	/* Don't do anything in the device though, leave it there: we don't yet
	 * have an obvious way to release PSIs.
	 * This means a PSI will be incorrectly marked as occupied, but that's
	 * not too bad: the PSI shuffling and its occupation counter are
	 * ephemeral, so they reset if you close and reopen all the volumes. */
	return;
}

/**
 * Synchronously store (and flush) the given posmap block
 *
 * MUTEX: @svol->posmap_lock must be held, except under volume ctor.
 */
static int store_posmap_block(struct sflite_volume *svol, u32 posmap_block_num)
{
	struct sflite_device *sdev = svol->sdev;
	struct page *page;
	struct bio *bio;
	int err;

	/* Sync + flush TODO GFP mask ok? */
	bio = bio_alloc_bioset(svol->dm_dev->bdev, 1,
			REQ_OP_WRITE | REQ_SYNC | REQ_FUA, GFP_NOIO,
			&sdev->bioset);
	if (!bio) {
		DMERR("Could not allocate posmap block bio");
		return -ENOMEM;
	}
	bio->bi_iter.bi_sector = SFLITE_POSMAP_START_SECTOR(svol) +
			(posmap_block_num << SFLITE_BLOCK_SHIFT);

	/* Alloc and add page TODO GFP mask */
	page = alloc_page(GFP_NOIO);
	if (!page) {
		DMERR("Could not allocate posmap block page");
		err = -ENOMEM;
		goto bad_alloc_page;
	}
	// TODO remove this error check
	if (unlikely(!bio_add_page(bio, page, SFLITE_BLOCK_SIZE, 0))) {
		DMCRIT("Could not add posmap block page to bio!");
		err = -ENOTRECOVERABLE;
		goto bad_add_page;
	}

	/* Serialise posmap block onto the page */
	void *page_ptr = kmap_local_page(page);
	u32 first_lsi = posmap_block_num * SFLITE_PSIS_PER_BLOCK;
	u32 last_lsi = min(first_lsi + SFLITE_PSIS_PER_BLOCK, sdev->tot_slices);
	u32 lsi;
	for (lsi = first_lsi; lsi < last_lsi; lsi++) {
		u32 psi = svol->posmap[lsi];
		__be32 *be_psi = (__be32*) (page_ptr + ((lsi-first_lsi) * sizeof(__be32)));
		*be_psi = cpu_to_be32(psi);
	}

//	print_hex_dump(KERN_WARNING, "page_ptr(REV)  ", DUMP_PREFIX_OFFSET, 32, 4, page_ptr, SFLITE_BLOCK_SIZE, false);
//	msleep(100);

	kunmap_local(page_ptr);

	/* Encrypt the block in place */
	err = sflite_crypt_block_page(svol->tfm, page, page,
			bio->bi_iter.bi_sector >> SFLITE_BLOCK_SHIFT, WRITE);
	if (err) {
		DMERR("Could not encrypt posmap block; error %d", err);
		goto bad_encrypt;
	}

	/* Submit */
	err = submit_bio_wait(bio);
	if (err)
		DMERR("Could not complete posmap block bio; error %d", err);

bad_encrypt:
bad_add_page:
	__free_page(page);
bad_alloc_page:
	bio_put(bio);
	return err;
}


/**
 * Create a new mapping for the given LSI, and synchronise back to disk.
 *
 * MUTEX: @svol->posmap_lock must be held, except under volume ctor.
 * MUTEX: takes @sdev->slices_lock.
 *
 * Syncing to disk means the posmap lock will be held (by the caller) for a long
 * time thus blocking out all the other incoming bio's, even unrelated ones
 * (falling in different slices). Several strategies are possible to avoid this
 * problem, but for now we keep this simple implementation.
 */
int sflite_create_persistent_slice_mapping(struct sflite_volume *svol, u32 lsi, u32 *psi)
{
	struct sflite_device *sdev = svol->sdev;
	int err;

	/* Bounds check TODO redundant? */
	if(unlikely(lsi >= svol->sdev->tot_slices))
		return -EINVAL;
	/* Check mapping not existent TODO redundant? */
	if (unlikely(svol->posmap[lsi] != SFLITE_PSI_INVALID))
		return -EINVAL;

	/* Create mapping */
	if (mutex_lock_interruptible(&sdev->slices_lock))
		return -ERESTARTSYS;
	err = peek_next_free_psi(sdev, psi);
	if (err) {
		mutex_unlock(&sdev->slices_lock);
		return err;
	}
	_create_local_slice_mapping(svol, lsi, *psi);
	mutex_unlock(&sdev->slices_lock);

	/* Write posmap block to disk */
	err = store_posmap_block(svol, lsi/SFLITE_PSIS_PER_BLOCK);
	if (err) {
		DMERR("Could not store posmap block; error %d", err);
		_delete_local_slice_mapping(svol, lsi);
		return err;
	}

	return 0;
}


/*
 *----------------------------
 * Load position map
 *----------------------------
 */

/**
 * Synchronously read the entire on-disk encrypted position map
 *
 * MUTEX: no need for the caller to hold @svol->posmap_lock (we are in ctor).
 */
static int read_encrypted_posmap(struct sflite_volume *svol)
{
	struct dm_io_request io_req = {
		.bi_opf = REQ_OP_READ | REQ_SYNC,
		.mem.type = DM_IO_VMA,
		.mem.ptr.vma = svol->posmap,
		.notify.fn = NULL,
		.client = svol->sdev->io_client
	};
	struct dm_io_region io_region = {
		.bdev = svol->dm_dev->bdev,
		.sector = SFLITE_POSMAP_START_SECTOR(svol),
		.count = svol->sdev->posmap_size_sectors
	};

	return sflc_dm_io(&io_req, 1, &io_region, NULL);
}

/**
 * De-serialise the position map entries. On the fly, if a conflict is detected,
 * resolve it by sampling a new PSI, and sync to disk (block by block).
 *
 * MUTEX: no need for the caller to hold @svol->posmap_lock (we are in ctor).
 * MUTEX: @sdev->slices_lock must be held.
 */
static int _deserialise_and_sanitise_posmap(struct sflite_volume *svol)
{
	struct sflite_device *sdev = svol->sdev;
	void *posmap_ptr = svol->posmap;
	u32 lsi;
	bool posmap_block_dirty;
	int err;

	for (lsi = 0; lsi < sdev->tot_slices; lsi++) {
		/* Reset dirty bit at the start of every posmap block */
		if (lsi % SFLITE_PSIS_PER_BLOCK == 0)
			posmap_block_dirty = false;

		/* De-serialise posmap entry */
		__be32 *be_psi = (__be32*) (posmap_ptr + (lsi * sizeof(__be32)));
		u32 psi = be32_to_cpu(*be_psi);

		/* If LSI unmapped, skip mapping creation */
		if (psi == SFLITE_PSI_INVALID) {
			svol->posmap[lsi] = psi;
			goto skip_create_mapping;
		}

		/* If PSI out of bounds, something's seriously wrong */
		if (psi >= sdev->tot_slices) {
			DMERR("Decrypted PSI out of bounds: %u >= %u", psi, sdev->tot_slices);
			return -EDOM;
		}

		/* If PSI already taken, sample a new one */
		if (sdev->slices_ofld[psi]) {
			DMWARN("Corruption of volume %u: LSI %u was evicted from PSI %u",
					svol->sv_base.vol_idx, lsi, psi);
			err = peek_next_free_psi(sdev, &psi);
			if (err)
				return err;
			posmap_block_dirty = true;
		}
		/* Whether sanitised or not, create the mapping locally */
		_create_local_slice_mapping(svol, lsi, psi);

skip_create_mapping:

		/* Only check dirty bit at the end of the posmap block */
		if (posmap_block_dirty &&
		    IS_LAST_LSI_IN_BLOCK(lsi, sdev)) {
			err = store_posmap_block(svol, lsi/SFLITE_PSIS_PER_BLOCK);
			if (err)
				return err;
		}
	}

	return 0;
}


/**
 * Load the volume's position map from the disk. If some conflicts are present
 * (i.e. an LSI is mapped to a PSI that's already taken), then resolve them
 * (i.e. re-sample a free PSI for the "unlucky" LSI) and sync back to disk.
 *
 * MUTEX: no need for the caller to hold @svol->posmap_lock (we are in ctor).
 * MUTEX: takes @sdev->slices_lock.
 */
int sflite_load_and_sanitise_posmap(struct sflite_volume *svol)
{
	int err;
	struct sflite_device *sdev = svol->sdev;

	/* Read raw posmap from disk */
	err = read_encrypted_posmap(svol);
	if (err)
		return err;

	/* Decrypt in place */
	err = sflite_crypt_blocks_vm(svol->tfm, svol->posmap, svol->posmap,
			svol->sdev->posmap_size_sectors >> SFLITE_BLOCK_SHIFT,
			SFLITE_POSMAP_START_SECTOR(svol) >> SFLITE_BLOCK_SHIFT,
			READ);
	if (err)
		return err;

	/* Deserialise and sanitise as you go */
	if (mutex_lock_interruptible(&sdev->slices_lock))
		return -ERESTARTSYS;
	err = _deserialise_and_sanitise_posmap(svol);
	mutex_unlock(&sdev->slices_lock);
	if (err)
		return err;

//	print_hex_dump(KERN_CRIT, "posmap(REV)    ", DUMP_PREFIX_OFFSET, 32, 4, svol->posmap, 4*sdev->tot_slices, false);
//	msleep(2000);

	return 0;
}

