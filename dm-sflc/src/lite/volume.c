/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/vmalloc.h>
#include "sflc_lite.h"
#include "sflc.h"


/**
 * Arguments:
 * 	argv[0]: Shufflecake mode: legacy/lite
 * 	argv[1]: Shufflecake-unique device ID
 * 	argv[2]: path to underlying physical device
 * 	argv[3]: volume index within the device
 * 	argv[4]: number of 1 MB slices in the underlying device
 *	argv[5]: 64-byte encryption key (hex-encoded)
 */
struct sflc_volume_base *sflite_vol_ctr(struct sflc_device_base *sd_base,
					struct dm_target *ti,
					int argc, char **argv)
{
	struct sflite_volume *svol;
	struct sflite_device *sdev = container_of(sd_base, struct sflite_device, sd_base);
	int err;

	svol = kzalloc(sizeof(*svol), GFP_KERNEL);
	if (!svol) {
		DMERR("Could not allocate volume");
		return ERR_PTR(-ENOMEM);
	}

	/* Init base part */
	err = sflc_vol_base_init(&svol->sv_base, sd_base, ti, argc, argv);
	if (err)
		goto bad_base;
	svol->sv_base.ops = &sflc_lite_ops;

	/* Parse arguments */
	if (argc != 6) {
		DMERR("Wrong argument count");
		err = -EINVAL;
		goto bad_parse;
	}
	/* Decode the encryption key */
	if (strlen(argv[5]) != 2 * SFLITE_XTS_KEYLEN) {
		DMERR("Invalid key length");
		err = -EINVAL;
		goto bad_parse;
	}
	err = hex2bin(svol->enckey, argv[5], SFLITE_XTS_KEYLEN);
	if (err) {
		DMERR("Could not decode hexadecimal encryption key");
		err = -EINVAL;
		goto bad_parse;
	}

	svol->sdev = sdev;

	svol->ti = ti;
	err = dm_get_device(ti, sdev->sd_base.name,
			dm_table_get_mode(ti->table), &svol->dm_dev);
	if (err) {
		ti->error = "Device lookup failed";
		goto bad_dm_dev;
	}

	/* Crypto */
	svol->tfm = crypto_alloc_skcipher("xts(aes)", 0, 0);
	if (IS_ERR(svol->tfm)) {
		err = PTR_ERR(svol->tfm);
		DMERR("Could not allocate AES-XTS cipher handle; error %d", err);
		goto bad_tfm_alloc;
	}
	err = crypto_skcipher_setkey(svol->tfm, svol->enckey, SFLITE_XTS_KEYLEN);
	if (err) {
		DMERR("Could not set key in crypto transform; error %d", err);
		goto bad_tfm_setkey;
	}

	/* Position map */
	mutex_init(&svol->posmap_lock);
	/* Slight over-allocation, to fit a whole number of blocks */
	svol->posmap = vmalloc(sdev->posmap_size_sectors * SECTOR_SIZE);
	if (!svol->posmap) {
		DMERR("Could not allocate position map");
		err = -ENOMEM;
		goto bad_posmap_alloc;
	}
	svol->nr_mapped_slices = 0;
	/* Load from disk */
	err = sflite_load_and_sanitise_posmap(svol);
	if (err) {
		DMERR("Could not load position map from disk; error %d", err);
		goto bad_posmap_load;
	}

	/* Add to sysfs, once initialised */
	err = sflite_sysfs_add_volume(svol);
	if (err) {
		DMERR("Could not register volume with sysfs; error %d", err);
		goto bad_sysfs;
	}

	/* Only accept one block per request for simplicity TODO: improve to one slice*/
	ti->max_io_len = SFLITE_BLOCK_SCALE;
	ti->flush_supported = true;
	ti->num_flush_bios = 1;
	ti->discards_supported = false;
	ti->num_discard_bios = 0;
	ti->num_secure_erase_bios = 0;
	ti->num_write_zeroes_bios = 0;
	ti->accounts_remapped_io = true;
	ti->per_io_data_size = sizeof(struct sflite_io);
	ti->private = svol;

	return &svol->sv_base;


bad_sysfs:
bad_posmap_load:
	vfree(svol->posmap);
bad_posmap_alloc:
bad_tfm_setkey:
	crypto_free_skcipher(svol->tfm);
bad_tfm_alloc:
	dm_put_device(ti, svol->dm_dev);
bad_dm_dev:
bad_parse:
	sflc_vol_base_exit(&svol->sv_base);
bad_base:
	kfree(svol);
	return ERR_PTR(err);
}


void sflite_vol_dtr(struct sflc_volume_base *sv_base)
{
	struct sflite_volume *svol = container_of(sv_base, struct sflite_volume, sv_base);

	sflite_sysfs_remove_volume(svol);
	vfree(svol->posmap);
	crypto_free_skcipher(svol->tfm);
	dm_put_device(svol->ti, svol->dm_dev);
	sflc_vol_base_exit(&svol->sv_base);
	kfree(svol);

	return;
}
