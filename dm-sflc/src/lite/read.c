/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sflc_lite.h"
#include <linux/delay.h>


static void sflite_read_endio(struct bio *phys_bio);
static void sflite_decrypt_work_fn(struct work_struct *work);


/* Landing here from ->map() through the io_queue */
void sflite_read_work_fn(struct work_struct *work)
{
	struct sflite_io *sio = container_of(work, struct sflite_io, work);
	struct sflite_volume *svol = sio->svol;
	struct sflite_device *sdev = svol->sdev;
	struct bio *orig_bio = sio->orig_bio;
	struct bio *phys_bio;
	u32 lsi = sio->lsi;
	u32 block_offset = sio->block_offset;
	u32 psi;

	/* Read position map */
	if (mutex_lock_interruptible(&svol->posmap_lock)) {
		orig_bio->bi_status = BLK_STS_IOERR;
		goto endio;
	}
	psi = svol->posmap[lsi];
	mutex_unlock(&svol->posmap_lock);

	/* If LSI is unmapped, short-circuit and return all zeros */
	if (psi == SFLITE_PSI_INVALID) {

		zero_fill_bio(orig_bio);
		orig_bio->bi_status = BLK_STS_OK;
		goto endio;
	}
	sio->psi = psi;

//	DMWARN("READ: LSI=%u, PSI=%u, offset=%u", lsi, psi, sio->block_offset);
//	msleep(100);

	/* Shallow-copy the bio and submit it (different bi_endio).
           We can shallow-copy because we don't need to own the pages,
           we can decrypt in place. */


	//DMWARN("READ: shallow copying");
	//msleep(500);

        /* Shallow copy */
        phys_bio = bio_alloc_clone(svol->dm_dev->bdev, orig_bio, GFP_NOIO, &sdev->bioset);
        if (!phys_bio) {
                DMERR("Could not clone original bio");
                orig_bio->bi_status = BLK_STS_IOERR;
		goto endio;
        }
        /* Insert in the I/O struct */
        sio->phys_bio = phys_bio;

//	DMWARN("READ: submitting bio");
//	msleep(500);

        /* Remap sector */
        phys_bio->bi_iter.bi_sector = SFLITE_PHYS_BIO_SECTOR(sdev, psi, block_offset);
        /* Set fields for the endio */
        phys_bio->bi_private = sio;
        phys_bio->bi_end_io = sflite_read_endio;
        /* Submit */
        dm_submit_bio_remap(orig_bio, phys_bio);

	return;


endio:
	bio_endio(orig_bio);
	return;
}


/* ISR for the phys_bio */
static void sflite_read_endio(struct bio *phys_bio)
{
	struct sflite_io *sio = phys_bio->bi_private;

//	DMWARN("READ ENDIO: queueing decryption");
//	//msleep(500);

	/* Can't decrypt here in ISR: submit to decryption workqueue.
	 * Can reuse the same work item, though, since it was popped out of the
	 * io_queue already */
	INIT_WORK(&sio->work, sflite_decrypt_work_fn);
	queue_work(sio->svol->sdev->crypt_queue, &sio->work);
}


/* Decrypt and endio */
static void sflite_decrypt_work_fn(struct work_struct *work)
{
	struct sflite_io *sio = container_of(work, struct sflite_io, work);
	struct sflite_volume *svol = sio->svol;
	struct bio *orig_bio = sio->orig_bio;
	struct bio *phys_bio = sio->phys_bio;
	struct bio_vec bvl = bio_iovec(orig_bio);
	int err;

	/* If physical bio failed, then fail-fast */
	if (phys_bio->bi_status != BLK_STS_OK) {
		orig_bio->bi_status = phys_bio->bi_status;
		goto endio;
	}

//	DMWARN("DECRYPT FN: decrypting page in place");
//	msleep(2000);

	/* Decrypt page in-place */
	err = sflite_crypt_block_page(svol->tfm, bvl.bv_page, bvl.bv_page,
		SFLITE_PHYS_BIO_SECTOR(svol->sdev, sio->psi, sio->block_offset) >> SFLITE_BLOCK_SHIFT,
		READ);
	if (err) {
		DMERR("Could not decrypt bio; error %d", err);
		orig_bio->bi_status = BLK_STS_IOERR;
		goto endio;
	}

//	print_hex_dump(KERN_WARNING, "readpage      ", DUMP_PREFIX_OFFSET, 32, 1, bvl.bv_page, SFLITE_BLOCK_SIZE, true);
//	msleep(2000);

//	DMWARN("DECRYPT FN: bio_advance");
//	msleep(300);

	/* Advance original bio by one block */
	bio_advance(orig_bio, SFLITE_BLOCK_SIZE);
	orig_bio->bi_status = BLK_STS_OK;

endio:
        /* Free the physical bio */
//	DMWARN("DECRYPT FN: bio_put");
//	msleep(300);
	bio_put(phys_bio);
	/* End original bio */
//	DMWARN("DECRYPT FN: bio_endio\n\n\n\n");
//	msleep(300);
        bio_endio(orig_bio);

	return;
}
