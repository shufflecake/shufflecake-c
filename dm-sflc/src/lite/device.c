/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/vmalloc.h>
#include <linux/math.h>
#include <linux/random.h>
#include <linux/minmax.h>
#include "sflc_lite.h"
#include "sflc.h"



/* Depth of the mempool backing the bio_set */
#define SFLITE_BIOSET_BIOS	64


/* Fisher-Yates shuffle */
static void fisheryates_u32(u32 *v, u32 len)
{
	u32 i, j;

	for (i = len-1; i >= 1; i--) {
		j = get_random_u32_below(i+1);
		swap(v[i], v[j]);
	}

	return;
}

/**
 * Arguments:
 * 	argv[0]: Shufflecake mode: legacy/lite
 * 	argv[1]: Shufflecake-unique device ID
 * 	argv[2]: path to underlying physical device
 * 	argv[3]: volume index within the device
 * 	argv[4]: number of 1 MB slices in the underlying device
 *	argv[5]: 64-byte encryption key (hex-encoded)
 */
struct sflc_device_base *sflite_dev_ctr(struct dm_target *ti, int argc, char **argv)
{
	struct sflite_device *sdev;
	u32 tot_slices;
	int i;
	int err;

	sdev = kzalloc(sizeof(*sdev), GFP_KERNEL);
	if (!sdev) {
		DMERR("Could not allocate device");
		return ERR_PTR(-ENOMEM);
	}

	/* Init base part */
	err = sflc_dev_base_init(&sdev->sd_base, ti, argc, argv);
	if (err)
		goto bad_base;
	sdev->sd_base.mode = SFLC_MODE_LITE;
	sdev->sd_base.ops = &sflc_lite_ops;

	/* Parse args */
	if (argc != 6) {
		DMERR("Wrong argument count");
		err = -EINVAL;
		goto bad_parse;
	}
	if (sscanf(argv[4], "%u", &tot_slices) != 1) {
		DMERR("Could not decode tot_slices");
		err = -EINVAL;
		goto bad_parse;
	}

	/* Compute sizes */
	sdev->tot_slices = tot_slices;
	sdev->nr_free_slices = tot_slices;
	/* Enough posmap blocks to fit all the entries */
	sdev->posmap_size_sectors = SFLITE_BLOCK_SCALE *
			DIV_ROUND_UP(tot_slices, SFLITE_PSIS_PER_BLOCK);
	/* DMB + VMBs + PosMaps */
	sdev->dev_header_size_sectors = SFLITE_BLOCK_SCALE +
			(SFLITE_DEV_MAX_VOLUMES * SFLITE_BLOCK_SCALE) +
			(SFLITE_DEV_MAX_VOLUMES * sdev->posmap_size_sectors);

	/* Shuffled PSIs */
	mutex_init(&sdev->slices_lock);
	sdev->slices_ofld = vzalloc(tot_slices * sizeof(bool));
	if (!sdev->slices_ofld) {
		DMERR("Could not allocate PSI occupation bitfield");
		err = -ENOMEM;
		goto bad_ofld;
	}

	sdev->prmslices = vmalloc(tot_slices * sizeof(u32));
	if (!sdev->prmslices) {
		DMERR("Could not allocate shuffled PSI array");
		err = -ENOMEM;
		goto bad_prmslices;
	}
	/* Generate a permutation */
	for (i = 0; i < tot_slices; i++)
		sdev->prmslices[i] = i;
	fisheryates_u32(sdev->prmslices, tot_slices);
	sdev->prmslices_octr = 0;

	/* Bioset */
	err = bioset_init(&sdev->bioset, SFLITE_BIOSET_BIOS, 0, BIOSET_NEED_BVECS);
	if (err) {
		DMERR("Could not init bioset; error %d", err);
		goto bad_bioset;
	}

	/* Client for dm-io */
	sdev->io_client = dm_io_client_create();
	if (IS_ERR(sdev->io_client)) {
		err = PTR_ERR(sdev->io_client);
		DMERR("Could not create dm-io client; error %d", err);
		goto bad_dmio_client;
	}

	/* I/O workqueue */
	sdev->io_queue = alloc_workqueue("sflite_%s_io",
			WQ_MEM_RECLAIM | WQ_CPU_INTENSIVE,
			0, sdev->sd_base.name);
	if (!sdev->io_queue) {
		err = -ENOMEM;
		DMERR("Could not allocate I/O workqueue");
		goto bad_io_wq;
	}
	/* Decryption workqueue */
	sdev->crypt_queue = alloc_workqueue("sflite_%s_crypt",
			WQ_MEM_RECLAIM | WQ_CPU_INTENSIVE,
			0, sdev->sd_base.name);
	if (!sdev->crypt_queue) {
		err = -ENOMEM;
		DMERR("Could not allocate decryption workqueue");
		goto bad_crypt_wq;
	}

	/* Add to sysfs, once initialised */
	err = sflite_sysfs_add_device(sdev);
	if (err)
		goto bad_sysfs;

	return &sdev->sd_base;


bad_sysfs:
	destroy_workqueue(sdev->crypt_queue);
bad_crypt_wq:
	destroy_workqueue(sdev->io_queue);
bad_io_wq:
	dm_io_client_destroy(sdev->io_client);
bad_dmio_client:
	bioset_exit(&sdev->bioset);
bad_bioset:
	vfree(sdev->prmslices);
bad_prmslices:
	vfree(sdev->slices_ofld);
bad_ofld:
bad_parse:
	sflc_dev_base_exit(&sdev->sd_base);
bad_base:
	kfree(sdev);
	return ERR_PTR(err);
}


void sflite_dev_dtr(struct sflc_device_base *sd_base)
{
	struct sflite_device *sdev = container_of(sd_base, struct sflite_device, sd_base);

	sflite_sysfs_remove_device(sdev);
	destroy_workqueue(sdev->crypt_queue);
	destroy_workqueue(sdev->io_queue);
	dm_io_client_destroy(sdev->io_client);
	bioset_exit(&sdev->bioset);
	vfree(sdev->prmslices);
	vfree(sdev->slices_ofld);
	sflc_dev_base_exit(&sdev->sd_base);
	kfree(sdev);

	return;
}

