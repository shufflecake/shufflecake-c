/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SFLITE_DMIOHELPER_H
#define _SFLITE_DMIOHELPER_H

#include <linux/dm-io.h>
#include <generated/uapi/linux/version.h>

/**
 * The function dm_io() has changed signature in recent kernels.
 * Here we provide a version-independent adapter, which uses a default value
 * for the fifth parameter (the new one).
 * The new signature is present for kernel 6.1.x with x>=83, 6.6.x with x>=23,
 * 6.7.x with x>=11, 6.8.x with x>=2, 6.x with x>=9
 */
#if LINUX_VERSION_MAJOR <= 5	// Old
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err)
#elif LINUX_VERSION_MAJOR >= 7	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
// Ok LINUX_VERSION_MAJOR is 6
#elif LINUX_VERSION_PATCHLEVEL >= 9	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
#elif LINUX_VERSION_PATCHLEVEL == 8 && LINUX_VERSION_SUBLEVEL >= 2	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
#elif LINUX_VERSION_PATCHLEVEL == 7 && LINUX_VERSION_SUBLEVEL >= 11	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
#elif LINUX_VERSION_PATCHLEVEL == 6 && LINUX_VERSION_SUBLEVEL >= 23	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
#elif LINUX_VERSION_PATCHLEVEL == 1 && LINUX_VERSION_SUBLEVEL >= 83	// New
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err, IOPRIO_DEFAULT)
#else	// Old
#define sflc_dm_io(ioreq, numreg, region, err)	dm_io(ioreq, numreg, region, err)
#endif

#endif	/* _SFLITE_DMIOHELPER_H */
