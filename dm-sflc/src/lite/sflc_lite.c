/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/device-mapper.h>
#include <linux/vmalloc.h>
#include "sflite_constants.h"
#include "sflc_lite.h"

// Only to import the definition of struct sflc_volume
#include "sflc.h"

#include <linux/delay.h>


/*
 *----------------------------
 * Device mapper target
 *----------------------------
 */

static int sflite_map(struct dm_target *ti, struct bio *bio)
{
	struct sflc_volume_base *sv_base = ti->private;
	struct sflite_volume *svol = container_of(sv_base, struct sflite_volume, sv_base);
	struct sflite_io *sio = dm_per_bio_data(bio, sizeof(struct sflite_io));
	sector_t lblk_num = bio->bi_iter.bi_sector >> SFLITE_BLOCK_SHIFT;

	if (unlikely(!bio_has_data(bio))) {
//        	DMWARN("No-data bio: bio_op() = %d, bi_opf = %u, bi_io_vec = %p, bi_idx = %u", bio_op(bio), bio->bi_opf, bio->bi_io_vec, bio->bi_iter.bi_idx);
//                msleep(100);
        }

	/* Flush requests are just passed down, since our position map is
	 * currently write-through, so we have no volatile cache */
	if (unlikely(bio->bi_opf & REQ_PREFLUSH)) {
		/* Has to be empty though */
		if (bio_sectors(bio)) {
			DMWARN("Non-empty flush request!");
			msleep(3000);
			return DM_MAPIO_KILL;
		}
//		DMWARN("REQ_PREFLUSH empty (phew), sector: %llu", bio->bi_iter.bi_sector);
//		msleep(100);
		bio_set_dev(bio, svol->dm_dev->bdev);
		return DM_MAPIO_REMAPPED;
	}

	/* Accept one block at a time TODO improve */
	if (unlikely(bio->bi_iter.bi_size > SFLITE_BLOCK_SIZE)) {
		DMWARN("Big bio: %u", bio->bi_iter.bi_size);
		msleep(300);
		dm_accept_partial_bio(bio, SFLITE_BLOCK_SCALE);
	}
	/* Only one segment, single page, starting at 0 TODO improve */
	if (unlikely(bio_segments(bio) > 1 ||
	    bio_offset(bio) != 0)) {
		DMWARN("Unaligned bio!");
		msleep(3000);
		return DM_MAPIO_KILL;
	}
	if (unlikely(bio->bi_iter.bi_size != SFLITE_BLOCK_SIZE)) {
		DMWARN("Wrong bio size: %u", bio->bi_iter.bi_size);
		msleep(3000);
		return DM_MAPIO_KILL;
	}

	/* Init I/O struct */
	sio->svol = svol;
	sio->orig_bio = bio;
	sio->lsi = lblk_num >> SFLITE_SLICE_SHIFT;
	sio->block_offset = lblk_num & ((1U << SFLITE_SLICE_SHIFT) - 1);

	/* Enqueue */
	if (bio_data_dir(bio) == READ)
		INIT_WORK(&sio->work, sflite_read_work_fn);
	else
		INIT_WORK(&sio->work, sflite_write_work_fn);
	queue_work(svol->sdev->io_queue, &sio->work);

	return DM_MAPIO_SUBMITTED;
}


static void sflite_io_hints(struct dm_target *ti, struct queue_limits *limits)
{
	// Currently, we only handle one block at a time TODO improve
	limits->logical_block_size = SFLITE_BLOCK_SIZE;
	limits->physical_block_size = SFLITE_BLOCK_SIZE;
	limits->io_min = SFLITE_BLOCK_SIZE;
	limits->io_opt = SFLITE_BLOCK_SIZE;

	return;
}


static int sflite_iterate_devices(struct dm_target *ti, iterate_devices_callout_fn fn, void *data)
{
	struct sflc_volume_base *sv_base = ti->private;
	struct sflite_volume *svol = container_of(sv_base, struct sflite_volume, sv_base);
	struct sflite_device *sdev = svol->sdev;

	if (!fn) {
		dump_stack();
		msleep(2000);
		return -EINVAL;
	}
	return fn(ti, svol->dm_dev, 0, sdev->dev_header_size_sectors + ti->len, data);
}


struct sflc_mode_ops sflc_lite_ops = {
	.dev_ctr		= sflite_dev_ctr,
	.dev_dtr		= sflite_dev_dtr,
	.vol_ctr		= sflite_vol_ctr,
	.vol_dtr		= sflite_vol_dtr,
	.map			= sflite_map,
	.io_hints		= sflite_io_hints,
	.iterate_devices	= sflite_iterate_devices,
};


/*
 *----------------------------
 * Init and exit
 *----------------------------
 */

int sflite_init(void)
{
	/* For the moment, we assume PAGE_SIZE == SFLITE_BLOCK_SIZE TODO improve */
	if (SFLITE_BLOCK_SIZE != PAGE_SIZE) {
		DMERR("Error, PAGE_SIZE != %d bytes not yet supported", SFLITE_BLOCK_SIZE);
		return -ENOTRECOVERABLE;
	}

	return 0;
}


void sflite_exit(void)
{
	return;
}

