/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/* Constants specific to Shufflecake Lite */

#ifndef _SFLITE_SFLITE_CONSTANTS_H_
#define _SFLITE_SFLITE_CONSTANTS_H_


#define SFLITE_BLOCK_SIZE	4096	/* bytes */
#define SFLITE_BLOCK_SHIFT	3
#define SFLITE_BLOCK_SCALE	(1 << SFLITE_BLOCK_SHIFT)	/* 8 sectors in a block */
#define SFLITE_SLICE_SHIFT	8
#define SFLITE_SLICE_SCALE	(1 << SFLITE_SLICE_SHIFT)	/* 256 blocks in a slice */


/* XTS requires doubling the key size */
#define SFLITE_XTS_KEYLEN	64	/* bytes */
/* The IV is the right-0-padded LE physical block number */
#define SFLITE_XTS_IVLEN	16 /* bytes */


#define SFLITE_DEV_MAX_VOLUMES	15
#define SFLITE_MAX_DEVS		1024


#define SFLITE_PSI_INVALID	0xFFFFFFFF
/* PosMap entries are 4 bytes, therefore there are 1024 of them in a block */
#define SFLITE_PSIS_PER_BLOCK	1024



#endif	/* _SFLITE_SFLITE_CONSTANTS_H_ */
