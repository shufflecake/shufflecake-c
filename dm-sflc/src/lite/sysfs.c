/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include "sflc_lite.h"

// Only to import the definitions of structs sflc_volume and sflc_device
#include "sflc.h"

/*
 *----------------------------
 * Device entries
 *----------------------------
 */

static ssize_t tot_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);
	struct sflite_device *sdev = container_of(sd_base, struct sflite_device, sd_base);

	return sysfs_emit(buf, "%u\n", sdev->tot_slices);
}

static ssize_t free_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);
	struct sflite_device *sdev = container_of(sd_base, struct sflite_device, sd_base);
	int ret;

	if (mutex_lock_interruptible(&sdev->slices_lock))
		return -ERESTARTSYS;
	ret = sysfs_emit(buf, "%u\n", sdev->nr_free_slices);
	mutex_unlock(&sdev->slices_lock);

	return ret;
}

static struct kobj_attribute tot_slices_kattr = __ATTR_RO(tot_slices);
static struct kobj_attribute free_slices_kattr = __ATTR_RO(free_slices);
static struct attribute *sflite_device_attrs[] = {
	&tot_slices_kattr.attr,
	&free_slices_kattr.attr,
	NULL
};
static const struct attribute_group sflite_device_attr_group = {
	.attrs = sflite_device_attrs,
};

int sflite_sysfs_add_device(struct sflite_device *sdev)
{
	return sysfs_create_group(&sdev->sd_base.kobj, &sflite_device_attr_group);
}

void sflite_sysfs_remove_device(struct sflite_device *sdev)
{
	sysfs_remove_group(&sdev->sd_base.kobj, &sflite_device_attr_group);
}


/*
 *----------------------------
 * Volume entries
 *----------------------------
 */

static ssize_t mapped_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_volume_base *sv_base = container_of(kobj, struct sflc_volume_base, kobj);
	struct sflite_volume *svol = container_of(sv_base, struct sflite_volume, sv_base);
	int ret;

	if (mutex_lock_interruptible(&svol->posmap_lock))
		return -ERESTARTSYS;
	ret = sysfs_emit(buf, "%u\n", svol->nr_mapped_slices);
	mutex_unlock(&svol->posmap_lock);

	return ret;
}

static struct kobj_attribute mapped_slices_kattr = __ATTR_RO(mapped_slices);
static struct attribute *sflite_volume_attrs[] = {
	&mapped_slices_kattr.attr,
	NULL
};
static const struct attribute_group sflite_volume_attr_group = {
	.attrs = sflite_volume_attrs,
};

int sflite_sysfs_add_volume(struct sflite_volume *svol)
{
	return sysfs_create_group(&svol->sv_base.kobj, &sflite_volume_attr_group);
}

void sflite_sysfs_remove_volume(struct sflite_volume *svol)
{
	sysfs_remove_group(&svol->sv_base.kobj, &sflite_volume_attr_group);
}
