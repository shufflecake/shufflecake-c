/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SFLITE_SFLITE_H
#define _SFLITE_SFLITE_H

#include <linux/device-mapper.h>
#include <linux/dm-io.h>
#include <linux/sysfs.h>
#include <crypto/skcipher.h>
#include "sflite_constants.h"
#include "sflc_types.h"
#include "sflc_constants.h"


/*
 *----------------------------
 * Structs
 *----------------------------
 */

struct sflite_device
{
	/* Base device object */
	struct sflc_device_base	sd_base;

	/* Logical size of each volume */
	u32			tot_slices;
	/* Header sizes in 512-byte sectors */
	sector_t		posmap_size_sectors;
	sector_t		dev_header_size_sectors;

	/* Shuffled array of PSIs */
	struct mutex		slices_lock;
	u32			*prmslices;
	u32			prmslices_octr;
	bool			*slices_ofld;
	u32			nr_free_slices;

	/* Resource sharing */
	struct bio_set		bioset;
	struct dm_io_client	*io_client;
	struct workqueue_struct *io_queue;
	struct workqueue_struct	*crypt_queue;
};

struct sflite_volume
{
	/* Base volume object */
	struct sflc_volume_base	sv_base;

	/* Backing device */
	struct sflite_device	*sdev;

	/* Underlying block device. This can't go in the sflite_device struct,
	 * because each ti grabs its own reference. */
	struct dm_dev		*dm_dev;
	struct dm_target	*ti;

	/* Position map */
	struct mutex		posmap_lock;
	u32			*posmap;
	u32			nr_mapped_slices;

	/* Crypto */
	u8			enckey[SFLITE_XTS_KEYLEN];
	struct crypto_skcipher	*tfm;
};

/* Per-bio data */
struct sflite_io
{
	struct sflite_volume	*svol;

	struct bio		*orig_bio;
	struct bio		*phys_bio;
	u32			lsi;
	u32			block_offset;
	u32			psi;

	struct work_struct	work;
};


/*
 *----------------------------
 * Macros
 *----------------------------
 */

/* Starting sector of position map */
#define SFLITE_POSMAP_START_SECTOR(svol) \
	(SFLITE_BLOCK_SCALE * (1 + SFLITE_DEV_MAX_VOLUMES) + \
	(svol)->sv_base.vol_idx * (svol)->sdev->posmap_size_sectors)


/* Physical sector of a remapped bio */
#define SFLITE_PHYS_BIO_SECTOR(sdev, psi, off) (				\
	(sdev)->dev_header_size_sectors	+ (				\
		((psi << SFLITE_SLICE_SHIFT) + off) << SFLITE_BLOCK_SHIFT	\
	)								\
)


/*
 *----------------------------
 * Public variables
 *----------------------------
 */

extern struct sflc_mode_ops sflc_lite_ops;


/*
 *----------------------------
 * Functions
 *----------------------------
 */

/* Init and exit */
int sflite_init(void);
void sflite_exit(void);

/* Device */
struct sflc_device_base *sflite_dev_ctr(struct dm_target *ti, int argc, char **argv);
void sflite_dev_dtr(struct sflc_device_base *sd_base);

/* Volume */
struct sflc_volume_base *sflite_vol_ctr(struct sflc_device_base *sd_base,
					struct dm_target *ti,
					int argc, char **argv);
void sflite_vol_dtr(struct sflc_volume_base *sv_base);

/* Sysfs */
int sflite_sysfs_add_device(struct sflite_device *sdev);
void sflite_sysfs_remove_device(struct sflite_device *sdev);
int sflite_sysfs_add_volume(struct sflite_volume *svol);
void sflite_sysfs_remove_volume(struct sflite_volume *svol);

/* Bio mapping */
void sflite_read_work_fn(struct work_struct *work);
void sflite_write_work_fn(struct work_struct *work);

/* Position map */
int sflite_load_and_sanitise_posmap(struct sflite_volume *svol);
int sflite_create_persistent_slice_mapping(struct sflite_volume *svol, u32 lsi, u32 *psi);

/* Crypto */
int sflite_crypt_blocks_vm(struct crypto_skcipher *tfm, void *src_buf, void *dst_buf,
		u64 num_blocks, u64 first_pblk_num, int rw);
int sflite_crypt_block_page(struct crypto_skcipher *tfm, struct page *src_page,
		struct page *dst_page, u64 pblk_num, int rw);


#endif	/* _SFLITE_SFLITE_H */
