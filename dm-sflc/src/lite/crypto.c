/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/scatterlist.h>
#include <linux/crypto.h>
#include <crypto/skcipher.h>
#include "sflite_constants.h"

#include "sflc_lite.h"


/**
 * Encrypt/decrypt exactly one block, already encoded in the scatterlist.
 * All other crypto functions reduce to this one.
 * The IV is constructed as the right-0-padded LE representation of the
 * physical block number, which is exactly what dm-crypt does when using the
 * IV mode "plain64".
 */
static int crypt_sg(struct crypto_skcipher *tfm, struct scatterlist *src,
		struct scatterlist *dst, u64 pblk_num, int rw)
{
	u8 iv[SFLITE_XTS_IVLEN];
	struct skcipher_request *req = NULL;
	DECLARE_CRYPTO_WAIT(wait);
	int err;

	// TODO not too sure about the gfp_mask here
	// TODO move @req into struct sflite_io?
	req = skcipher_request_alloc(tfm, GFP_NOIO);
	if (!req)
		return -ENOMEM;

	skcipher_request_set_callback(req,
		CRYPTO_TFM_REQ_MAY_BACKLOG | CRYPTO_TFM_REQ_MAY_SLEEP,
		crypto_req_done, &wait);

	/* Construct IV */
	memset(iv, 0, SFLITE_XTS_IVLEN);
	*(__le64 *)iv = cpu_to_le64(pblk_num);

	skcipher_request_set_crypt(req, src, dst, SFLITE_BLOCK_SIZE, iv);
	if (rw == READ)
		err = crypto_wait_req(crypto_skcipher_decrypt(req), &wait);
	else
		err = crypto_wait_req(crypto_skcipher_encrypt(req), &wait);
	skcipher_request_free(req);

	return err;
}

/* Encrypt-decrypt a single block (memory buffer is a page) */
int sflite_crypt_block_page(struct crypto_skcipher *tfm, struct page *src_page,
		struct page *dst_page, u64 pblk_num, int rw)
{
	struct scatterlist dst, src, *p_dst;
	bool is_inplace;

	/* Use same scatterlist if in-place */
	is_inplace = (src_page == dst_page);
	p_dst = is_inplace ? &src : &dst;

	/* We assume PAGE_SIZE == SFLITE_BLOCK_SIZE */
	/* And orig_bio to start at offset 0 within the page */
	sg_init_table(&src, 1);
	sg_set_page(&src, src_page, SFLITE_BLOCK_SIZE, 0);
	if (!is_inplace) {
		sg_init_table(&dst, 1);
		sg_set_page(&dst, dst_page, SFLITE_BLOCK_SIZE, 0);
	}

	return crypt_sg(tfm, &src, p_dst, pblk_num, rw);
}


/* Encrypt-decrypt consecutive blocks (memory buffer is vmalloc'ed) */
int sflite_crypt_blocks_vm(struct crypto_skcipher *tfm, void *src_buf, void *dst_buf,
		u64 num_blocks, u64 first_pblk_num, int rw)
{
	struct scatterlist dst, src, *p_dst;
	u64 pblk_num;
	bool is_inplace;
	int err;

	/* Use same scatterlist if in-place */
	is_inplace = (src_buf == dst_buf);
	p_dst = is_inplace ? &src : &dst;

	for (pblk_num = first_pblk_num;
	    pblk_num < first_pblk_num + num_blocks;
	    pblk_num++) {
		sg_init_one(&src, src_buf, SFLITE_BLOCK_SIZE);
		if (!is_inplace)
			sg_init_one(&dst, dst_buf, SFLITE_BLOCK_SIZE);

		err = crypt_sg(tfm, &src, p_dst, pblk_num, rw);
		if (err)
			return err;

		src_buf += SFLITE_BLOCK_SIZE;
		dst_buf += SFLITE_BLOCK_SIZE;
	}

	return 0;
}

