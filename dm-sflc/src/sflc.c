/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/device-mapper.h>
#include <linux/vmalloc.h>

#include "sflc.h"
#include "legacy/sflc_legacy.h"
#include "lite/sflc_lite.h"

#include <linux/delay.h>


/* Global variables */

// A set of algorithms for each Shufflecake mode, in the same order as the modes.
struct sflc_mode_ops *sflc_all_mode_ops[SFLC_NR_MODES] = {
	&sflc_legacy_ops,
	&sflc_lite_ops
};

// Global array of base device objects
DEFINE_MUTEX(sflc_alldevs_lock);
struct sflc_device_base **sflc_alldevs = NULL;
u32 sflc_free_devid = 0;	/* The lowest free devID */


/* Add a device to the global array, and advance next_dev_id */
static int sflc_add_device_global(u32 dev_id, struct sflc_device_base *sd_base)
{
	int i;

	if (mutex_lock_interruptible(&sflc_alldevs_lock))
		return -ERESTARTSYS;

	/* Check for dev_id conflict, possible because a read() to next_dev_id
	 * in sysfs can return the same value to two processes */
	if (sflc_alldevs[dev_id]) {
		mutex_unlock(&sflc_alldevs_lock);
		DMERR("A device with this ID already exists. Retry");
		return -EINVAL;
	}
	// Add to the global array, and advance free_devid
	sflc_alldevs[dev_id] = sd_base;
	for (i = sflc_free_devid; i < SFLC_MAX_DEVS && sflc_alldevs[i]; i++);
	sflc_free_devid = i;

	mutex_unlock(&sflc_alldevs_lock);

	return 0;
}

/* Remove a device from the global array, and update next_dev_id */
static void sflc_remove_device_global(u32 dev_id)
{
	if (mutex_lock_interruptible(&sflc_alldevs_lock))
		return;
	sflc_alldevs[dev_id] = NULL;
	if (dev_id < sflc_free_devid)
		sflc_free_devid = dev_id;
	mutex_unlock(&sflc_alldevs_lock);
}


/*
 * Create volume and, if not existent, the underlying device.
 * Arguments:
 * 	argv[0]: Shufflecake mode: legacy/lite
 * 	argv[1]: Shufflecake-unique device ID
 * 	argv[2]: path to underlying physical device
 * 	argv[3]: volume index within the device
 * 	argv[4:]: mode-specific parameters
 */
static int sflc_ctr(struct dm_target *ti, unsigned int argc, char **argv)
{
	u32 mode;
	struct sflc_mode_ops *mode_ops;
	u32 dev_id;
	u32 vol_idx;
	struct sflc_device_base *sd_base;
	struct sflc_volume_base *sv_base;
	int err;

	/* Parse arguments */
	if (argc < 4) {
		ti->error = "Invalid argument count";
		return -EINVAL;
	}
	if (sscanf(argv[0], "%u", &mode) != 1) {
		ti->error = "Could not decode mode";
		return -EINVAL;
	}
	if (sscanf(argv[1], "%u", &dev_id) != 1) {
		ti->error = "Could not decode device ID";
		return -EINVAL;
	}
	if (sscanf(argv[3], "%u", &vol_idx) != 1) {
		ti->error = "Could not decode volume index";
		return -EINVAL;
	}
	/* Sanity checks */
	if (mode >= SFLC_NR_MODES) {
		ti->error = "Mode out of bounds";
		return -EINVAL;
	}
	if (dev_id >= SFLC_MAX_DEVS) {
		ti->error = "Device ID out of bounds";
		return -EINVAL;
	}
	if (vol_idx >= SFLC_DEV_MAX_VOLUMES) {
		ti->error = "Volume index out of bounds";
		return -EINVAL;
	}
	/* Grab the correct mode */
	mode_ops = sflc_all_mode_ops[mode];

	/* Create device, if this is the first volume, otherwise retrieve it */
	if (vol_idx == 0) {
		sd_base = mode_ops->dev_ctr(ti, argc, argv);
		if (IS_ERR(sd_base)) {
			ti->error = "Could not instantiate device";
			return PTR_ERR(sd_base);
		}
		/* Insert in global array */
		err = sflc_add_device_global(dev_id, sd_base);
		if (err) {
			ti->error = "Could not add device to global array";
			goto bad_dev_global;
		}
	} else {
		if (mutex_lock_interruptible(&sflc_alldevs_lock))
			return -ERESTARTSYS;
		sd_base = sflc_alldevs[dev_id];
		mutex_unlock(&sflc_alldevs_lock);

		if (!sd_base) {
			ti->error = "Could not find device with specified ID";
			return -EINVAL;
		}
	}

	/* Create volume */
	sv_base = mode_ops->vol_ctr(sd_base, ti, argc, argv);
	if (IS_ERR(sv_base)) {
		ti->error = "Could not instantiate volume";
		err = PTR_ERR(sv_base);
		goto bad_vol_create;
	}
	/* We expect ->ctr() calls to be strictly sequential, so we don't need locking */
	sd_base->nr_volumes++;

	ti->private = sv_base;

	return 0;


bad_vol_create:
	if (vol_idx == 0) {
		sflc_remove_device_global(dev_id);
bad_dev_global:
		mode_ops->dev_dtr(sd_base);
	}
	return err;
}


/* Destroy volume and, if needed, the underlying device */
static void sflc_dtr(struct dm_target *ti)
{
	struct sflc_volume_base *sv_base = ti->private;
	struct sflc_device_base *sd_base = sv_base->sd_base;

	sv_base->ops->vol_dtr(sv_base);
	/* We expect ->dtr() calls to be strictly sequential, so we don't need locking */
	sd_base->nr_volumes--;

	if (sd_base->nr_volumes == 0) {
		sflc_remove_device_global(sd_base->dev_id);
		sd_base->ops->dev_dtr(sd_base);
	}

	return;
}


static int sflc_map(struct dm_target *ti, struct bio *bio)
{
	struct sflc_volume_base *sv_base = ti->private;
	return sv_base->ops->map(ti, bio);
}

static void sflc_io_hints(struct dm_target *ti, struct queue_limits *limits)
{
	struct sflc_volume_base *sv_base = ti->private;
	return sv_base->ops->io_hints(ti, limits);
	return;
}

static int sflc_iterate_devices(struct dm_target *ti, iterate_devices_callout_fn fn, void *data)
{
	struct sflc_volume_base *sv_base = ti->private;
	return sv_base->ops->iterate_devices(ti, fn, data);
}


/*
 *----------------------------
 * Kernel module
 *----------------------------
 */

static struct target_type sflc_target_type = {
	.name			= SFLC_TARGET_NAME,
	.version		= {SFLC_VER_MAJOR, SFLC_VER_MINOR, SFLC_VER_REVISION},
	.module			= THIS_MODULE,
	.ctr			= sflc_ctr,
	.dtr			= sflc_dtr,
	.map			= sflc_map,
	.io_hints		= sflc_io_hints,
	.iterate_devices	= sflc_iterate_devices,
};


/* Module entry point */
static int sflc_init(void)
{
	int ret;

	sflc_alldevs = vzalloc(SFLC_MAX_DEVS * sizeof(*sflc_alldevs));
	if (!sflc_alldevs) {
		DMERR("Could not allocate sflc_alldevs");
		ret = -ENOMEM;
		goto bad_alldevs_alloc;
	}

	/* Create the first sysfs entries */
	ret = sflc_sysfs_init();
	if (ret)
		goto err_sysfs;

	/* Init the Legacy module */
	ret = sflegc_init();
	if (ret)
		goto err_sflegc;

	/* Init the Lite module */
	ret = sflite_init();
	if (ret)
		goto err_sflite;

	/* Register the DM callbacks */
	ret = dm_register_target(&sflc_target_type);
	if (ret < 0)
		goto err_dm;

	DMINFO("loaded");
	return 0;


err_dm:
	sflite_exit();
err_sflite:
	sflegc_exit();
err_sflegc:
	sflc_sysfs_exit();
err_sysfs:
	vfree(sflc_alldevs);
bad_alldevs_alloc:
	DMERR("not loaded");
	return ret;
}


/* Module exit point */
static void sflc_exit(void)
{
	dm_unregister_target(&sflc_target_type);
	sflegc_exit();
	sflite_exit();
	sflc_sysfs_exit();
	vfree(sflc_alldevs);

	DMINFO("unloaded");
	return;
}


module_init(sflc_init);
module_exit(sflc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Toninov");
