/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/*
 * A volume represents a single "virtual" block device, mapping onto
 * a "real" device represented by a device.
 */

#ifndef _SFLEGC_VOLUME_VOLUME_H_
#define _SFLEGC_VOLUME_VOLUME_H_


/*****************************************************
 *             TYPES FORWARD DECLARATIONS            *
 *****************************************************/

/* Necessary since device.h, volume.h, and sysfs.h all include each other */

typedef struct sflegc_vol_write_work_s sflegc_vol_WriteWork;
typedef struct sflegc_vol_decrypt_work_s sflegc_vol_DecryptWork;
typedef struct sflegc_volume_s sflegc_Volume;


/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/blk_types.h>

#include "sflc_types.h"
#include "legacy/sflc_legacy.h"
#include "legacy/device/device.h"
#include "legacy/crypto/symkey/symkey.h"


/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/* A single header data block contains 1024 fmap mappings */
#define SFLEGC_VOL_HEADER_MAPPINGS_PER_BLOCK (SFLEGC_DEV_SECTOR_SIZE / sizeof(u32))

/* We split the volume's logical addressing space into 1 MB slices */
#define SFLEGC_VOL_LOG_SLICE_SIZE 256	// In 4096-byte sectors

/* Value marking an LSI as unassigned */
#define SFLEGC_VOL_FMAP_INVALID_PSI 0xFFFFFFFFU

/* The volume name is "sflc-<devID>-<volIdx>" */
#define SFLEGC_VOL_NAME_MAX_LEN 12


/*****************************************************
 *                       TYPES                       *
 *****************************************************/

struct sflegc_vol_write_work_s
{
	/* Essential information */
        sflegc_Volume           * vol;
        struct bio            * orig_bio;

	/* Write requests need to allocate own page */
	struct page	      * page;

	/* Will be submitted to workqueue */
        struct work_struct      work;
};

struct sflegc_vol_decrypt_work_s
{
	/* Essential information */
        sflegc_Volume           * vol;
        struct bio            * orig_bio;
	struct bio	      * phys_bio;

	/* IV retrieval information */
	u32 			psi;
	u32			off_in_slice;

	/* Will be submitted to workqueue */
        struct work_struct      work;
};

struct sflegc_volume_s
{
	/* Base volume object */
	struct sflc_volume_base		sv_base;

	/* Backing device */
	sflegc_Device    		      * dev;

	/* Forward position map */
	struct mutex			fmap_lock;
	u32	        	      *	fmap;
	/* Stats on the fmap */
	u32				mapped_slices;


	/* Crypto */
	sflegc_sk_Context  	      *	skctx;
};


/*****************************************************
 *            PUBLIC FUNCTIONS PROTOTYPES            *
 *****************************************************/

struct sflc_volume_base *sflegc_vol_ctr(struct sflc_device_base *sd_base,
					struct dm_target *ti,
					int argc, char **argv);
void sflegc_vol_dtr(struct sflc_volume_base *sv_base);

/* Remaps the underlying block device and the sector number */
int sflegc_vol_remapBioFast(sflegc_Volume * vol, struct bio * bio);
/* Processes the bio in the normal indirection+crypto way */
int sflegc_vol_processBio(sflegc_Volume * vol, struct bio * bio);

/* Executed in top half */
void sflegc_vol_doRead(sflegc_Volume * vol, struct bio * bio);
/* Executed in bottom half */
void sflegc_vol_doWrite(struct work_struct * work);

/* Maps a logical 512-byte sector to a physical 512-byte sector. Returns < 0 if error.
 * Specifically, if op == READ, and the logical slice is unmapped, -ENXIO is returned. */
s64 sflegc_vol_remapSector(sflegc_Volume * vol, sector_t log_sector, int op, u32 * psi_out, u32 * off_in_slice_out);
/* Loads (and decrypts) the position map from the volume's header */
int sflegc_vol_loadFmap(sflegc_Volume * vol);
/* Stores (and encrypts) the position map to the volume's header */
int sflegc_vol_storeFmap(sflegc_Volume * vol);


#endif /* _SFLEGC_VOLUME_VOLUME_H_ */
