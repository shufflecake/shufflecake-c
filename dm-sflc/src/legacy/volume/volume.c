/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/*
 * This file only implements the volume creation and destruction functions.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "legacy/volume/volume.h"
#include "legacy/utils/string.h"
#include "legacy/log/log.h"
#include "sflc.h"

#include <linux/vmalloc.h>


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/**
 * Creates volume. Returns an ERR_PTR() if unsuccessful
 * Arguments:
 * 	argv[0]: Shufflecake mode: legacy/lite
 * 	argv[1]: Shufflecake-unique device ID
 * 	argv[2]: path to underlying physical device
 * 	argv[3]: volume index within the device
 * 	argv[4]: number of 1 MB slices in the underlying device
 *	argv[5]: 32-byte encryption key (hex-encoded)
 */
struct sflc_volume_base *sflegc_vol_ctr(struct sflc_device_base *sd_base,
					struct dm_target *ti,
					int argc, char **argv)
{
	sflegc_Volume * vol;
	sflegc_Device *dev = container_of(sd_base, sflegc_Device, sd_base);
	u8 enckey[SFLEGC_SK_KEY_LEN];
	int err;

	/* Allocate volume */
	vol = kzalloc(sizeof(sflegc_Volume), GFP_KERNEL);
	if (!vol) {
		pr_err("Could not allocate %lu bytes for sflegc_Volume\n", sizeof(sflegc_Volume));
		err = -ENOMEM;
		goto err_alloc_vol;
	}

	/* Init base part */
	err = sflc_vol_base_init(&vol->sv_base, sd_base, ti, argc, argv);
	if (err)
		goto bad_base;
	vol->sv_base.ops = &sflc_legacy_ops;

	/* Parse args */
	if (argc != 6) {
		pr_err("Wrong argument count");
		err = -EINVAL;
		goto err_parse;
	}
	/* Decode the encryption key */
	if (strlen(argv[5]) != 2 * SFLEGC_SK_KEY_LEN) {
		pr_err("Hexadecimal key (length %lu): %s\n", strlen(argv[5]), argv[5]);
		err = -EINVAL;
		goto err_parse;
	}
	err = sflegc_str_hexDecode(argv[5], enckey);
	if (err) {
		pr_err("Could not decode hexadecimal encryption key");
		err = -EINVAL;
		goto err_parse;
	}

	/* Sysfs stuff */
	err = sflegc_sysfs_add_volume(vol);
	if (err) {
		pr_err("Could not add volume to sysfs; error %d\n", err);
		goto err_sysfs;
	}

	/* Backing device */
	if (!sflegc_dev_addVolume(dev, vol, vol->sv_base.vol_idx)) {
		pr_err("Could not add volume to device\n");
		err = -EINVAL;
		goto err_add_to_dev;
	}
	vol->dev = dev;

	/* Crypto */
	vol->skctx = sflegc_sk_createContext(enckey);
	if (IS_ERR(vol->skctx)) {
		err = PTR_ERR(vol->skctx);
		pr_err("Could not create crypto context\n");
		goto err_create_skctx;
	}

	/* Initialise fmap_lock */
	mutex_init(&vol->fmap_lock);
	/* Allocate forward map */
	vol->fmap = vmalloc(dev->tot_slices * sizeof(u32));
	if (!vol->fmap) {
		pr_err("Could not allocate forward map\n");
		err = -ENOMEM;
		goto err_alloc_fmap;
	}
	/* And init the stats */
	vol->mapped_slices = 0;

	/* Initialise fmap */
	err = sflegc_vol_loadFmap(vol);
	if (err) {
		pr_err("Could not load position map; error %d\n", err);
		goto err_load_fmap;
	}


	/* Tell DM we want one SFLC sector at a time */
	ti->max_io_len = SFLEGC_DEV_SECTOR_SCALE;
	/* Enable REQ_OP_FLUSH bios */
	ti->num_flush_bios = 1;
	/* Disable REQ_OP_WRITE_ZEROES and REQ_OP_SECURE_ERASE (can't be passed through as
	   they would break deniability, and they would be too complicated to handle individually) */
	ti->num_secure_erase_bios = 0;
	ti->num_write_zeroes_bios = 0;
	/* Momentarily disable REQ_OP_DISCARD_BIOS
	   TODO: will need to support them to release slice mappings */
	ti->num_discard_bios = 0;

	return &vol->sv_base;


err_load_fmap:
	vfree(vol->fmap);
err_alloc_fmap:
	sflegc_sk_destroyContext(vol->skctx);
err_create_skctx:
	sflegc_dev_removeVolume(vol->dev, vol->sv_base.vol_idx);
err_add_to_dev:
	sflegc_sysfs_remove_volume(vol);
err_sysfs:
err_parse:
	sflc_vol_base_exit(&vol->sv_base);
bad_base:
	kfree(vol);
err_alloc_vol:
	return ERR_PTR(err);
}

/* Removes the volume from the device and frees it. */
void sflegc_vol_dtr(struct sflc_volume_base *sv_base)
{
	sflegc_Volume *vol = container_of(sv_base, sflegc_Volume, sv_base);
	int err;

	/* Store fmap */
	err = sflegc_vol_storeFmap(vol);
	if (err) {
		pr_err("Could not store position map; error %d\n", err);
	}
	/* Free it */
	vfree(vol->fmap);

	/* Skctx */
	sflegc_sk_destroyContext(vol->skctx);

	/* Remove from device */
	sflegc_dev_removeVolume(vol->dev, vol->sv_base.vol_idx);

	/* Destroy sysfs entries */
	sflegc_sysfs_remove_volume(vol);

	/* Base part */
	sflc_vol_base_exit(&vol->sv_base);

	/* Free volume structure */
	kfree(vol);

	return;
}
