/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/module.h>
#include <linux/device-mapper.h>

#include "legacy/sflc_legacy.h"
#include "legacy/crypto/symkey/symkey.h"
#include "legacy/crypto/rand/rand.h"
#include "legacy/utils/pools.h"
#include "legacy/utils/workqueues.h"
#include "legacy/log/log.h"


/*****************************************************
 *            MODULE FUNCTIONS DEFINITIONS           *
 *****************************************************/

/* Module entry point, called just once, at module-load time */
int sflegc_init(void)
{
	int ret;

	ret = sflegc_rand_init();
	if (ret) {
		pr_err("Could not init rand; error %d\n", ret);
		goto err_rand_init;
	}

	/* Init the memory pools */
	ret = sflegc_pools_init();
	if (ret) {
		pr_err("Could not init memory pools; error %d\n", ret);
		goto err_pools;
	}

	/* Init the workqueues */
	ret = sflegc_queues_init();
	if (ret) {
		pr_err("Could not init workqueues; error %d\n", ret);
		goto err_queues;
	}

	return 0;


err_queues:
	sflegc_pools_exit();
err_pools:
	sflegc_rand_exit();
err_rand_init:
	return ret;
}

/* Module exit point, called just once, at module-unload time */
void sflegc_exit(void)
{
	sflegc_queues_exit();
	sflegc_pools_exit();
	sflegc_rand_exit();

	return;
}
