/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef _SFLEGC_SFLEGC_H
#define _SFLEGC_SFLEGC_H


// For the definition of sflegc_Device and its functions
#include "legacy/device/device.h"
// For the definition of sflegc_Volume and its functions
#include "legacy/volume/volume.h"


extern struct sflc_mode_ops sflc_legacy_ops;


int sflegc_init(void);
void sflegc_exit(void);

int sflegc_sysfs_add_device(sflegc_Device *dev);
void sflegc_sysfs_remove_device(sflegc_Device *dev);
int sflegc_sysfs_add_volume(sflegc_Volume *vol);
void sflegc_sysfs_remove_volume(sflegc_Volume *vol);


#endif	/* _SFLEGC_SFLEGC_H */
