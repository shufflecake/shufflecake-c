/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <crypto/rng.h>
#include <linux/random.h>

#include "legacy/crypto/rand/rand.h"
#include "legacy/log/log.h"

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

#define SFLEGC_RAND_RNG_NAME "drbg_nopr_sha256"

/*****************************************************
 *                 PRIVATE VARIABLES                 *
 *****************************************************/

static struct mutex sflegc_rand_tfm_lock;
static struct crypto_rng * sflegc_rand_tfm = NULL;

/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

/* Flexible to accommodate for both required and non-required reseeding */
static int sflegc_rand_reseed(void);

/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Init the submodule */
int sflegc_rand_init(void)
{
	int err;

	/* Init the lock governing the SFLC RNG */
	mutex_init(&sflegc_rand_tfm_lock);

	/* Allocate module-wide RNG */
	sflegc_rand_tfm = crypto_alloc_rng(SFLEGC_RAND_RNG_NAME, CRYPTO_ALG_TYPE_RNG, 0);
	if (IS_ERR(sflegc_rand_tfm)) {
		err = PTR_ERR(sflegc_rand_tfm);
		sflegc_rand_tfm = NULL;
		pr_err("Could not allocate RNG %s; error %d\n", SFLEGC_RAND_RNG_NAME, err);
		return err;
	}

	/* The new RNG comes not seeded, right? */
	err = sflegc_rand_reseed();
	if (err) {
		pr_err("Could not seed the RNG; error %d\n", err);
		sflegc_rand_exit();
		return err;
	}

	return 0;
}

/* Get random bytes. Might sleep for re-seeding (not implemented yet), or for contention (mutex). */
int sflegc_rand_getBytes(u8 * buf, unsigned count)
{
	int ret;

	/* Acquire lock */
	if (mutex_lock_interruptible(&sflegc_rand_tfm_lock)) {
		pr_err("Got error while waiting for SFLC RNG\n");
		return -EINTR;
	}

	ret = crypto_rng_get_bytes(sflegc_rand_tfm, buf, count);

	/* End of critical region */
	mutex_unlock(&sflegc_rand_tfm_lock);

	return ret;
}

/* Get a random s32 from 0 (inclusive) to max (exclusive). Returns < 0 if error. */
s32 sflegc_rand_uniform(s32 max)
{
	s32 rand;
	s32 thresh;

	/* Sanity check */
	if (max <= 0) {
		pr_err("Can't have negative number as upper limit");
		return -EINVAL;
	}

	/* We'll basically do rand % max, but to avoid
	   skewing the distribution we have to exclude the
	   highest rand's (in absolute value). */
	thresh = S32_MAX - (S32_MAX % max);
	do {
		/* Sample a random signed integer, then make it positive */
		int err = sflegc_rand_getBytes((void *) &rand, sizeof(rand));
		/* Can't make it positive if it's all 1's */
		if (rand == S32_MIN) {
			continue;
		}
		/* Make it positive */
		if (rand < 0) {
			rand = -rand;
		}

		if (err) {
			pr_err("Got error when sampling random number");
			return err;
		}
	} while(rand >= thresh);

	return rand % max;
}

/* Tear down the submodule */
void sflegc_rand_exit(void)
{
	if (sflegc_rand_tfm) {
		crypto_free_rng(sflegc_rand_tfm);
		sflegc_rand_tfm = NULL;
	}

	return;
}

/*****************************************************
 *           PRIVATE FUNCTIONS DEFINITIONS           *
 *****************************************************/

/* Flexible to accommodate for both required and non-required reseeding */
static int sflegc_rand_reseed(void)
{
	int err;

	/* Reseed the RNG */
	err = crypto_rng_reset(sflegc_rand_tfm, NULL, crypto_rng_seedsize(sflegc_rand_tfm));
	if (err) {
		pr_err("Could not feed seed to the RNG; error %d\n", err);
		return err;
	}

	return 0;
}
