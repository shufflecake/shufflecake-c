/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/*
 * A module-wide source of randomness.
 * Private to Shufflecake (no randomness sharing via /dev/urandom), but
 * no need to make it more fine grained.
 */

#ifndef _SFLEGC_CRYPTO_RAND_RAND_H_
#define _SFLEGC_CRYPTO_RAND_RAND_H_

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/types.h>

/*****************************************************
 *            PUBLIC FUNCTIONS PROTOTYPES            *
 *****************************************************/

/* Init the submodule */
int sflegc_rand_init(void);

/* Get random bytes. Might sleep for re-seeding (not implemented yet), or for contention (mutex). */
int sflegc_rand_getBytes(u8 * buf, unsigned count);

/* Get a random s32 from 0 (inclusive) to max (exclusive). Returns < 0 if error. */
s32 sflegc_rand_uniform(s32 max);

/* Tear down the submodule */
void sflegc_rand_exit(void);


#endif /* _SFLEGC_CRYPTO_RAND_RAND_H_ */
