/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * Methods of our DM target
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "legacy/device/device.h"
#include "legacy/volume/volume.h"
#include "legacy/utils/bio.h"
#include "legacy/utils/string.h"
#include "legacy/log/log.h"

// Only to import the definition of struct sflc_volume
#include "sflc.h"

/*****************************************************
 *                    CONSTANTS                      *
 *****************************************************/

/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

static int sflegc_tgt_map(struct dm_target *ti, struct bio *bio);
static void sflegc_tgt_ioHints(struct dm_target *ti, struct queue_limits *limits);
static int sflegc_tgt_iterateDevices(struct dm_target *ti, iterate_devices_callout_fn fn,
									void *data);

/*****************************************************
 *           PUBLIC VARIABLES DEFINITIONS            *
 *****************************************************/

struct sflc_mode_ops sflc_legacy_ops = {
	.dev_ctr		= sflegc_dev_ctr,
	.dev_dtr		= sflegc_dev_dtr,
	.vol_ctr		= sflegc_vol_ctr,
	.vol_dtr		= sflegc_vol_dtr,
	.map			= sflegc_tgt_map,
	.io_hints		= sflegc_tgt_ioHints,
	.iterate_devices	= sflegc_tgt_iterateDevices,
};

/*****************************************************
 *           PRIVATE FUNCTIONS DEFINITIONS           *
 *****************************************************/


/* Callback for every bio submitted to our virtual block device */
static int sflegc_tgt_map(struct dm_target *ti, struct bio *bio) 
{
	int err;
	struct sflc_volume_base *sv_base = ti->private;
	sflegc_Volume *vol = container_of(sv_base, sflegc_Volume, sv_base);

        /* Copied from sflc_lite */
	if (unlikely(bio->bi_opf & REQ_PREFLUSH)) {
		/* Has to be empty though */
		if (bio_sectors(bio)) {
			DMWARN("Non-empty flush request!");
			//msleep(3000);
			return DM_MAPIO_KILL;
		}
//		DMWARN("REQ_PREFLUSH empty (phew), sector: %llu", bio->bi_iter.bi_sector);
//		msleep(100);
		bio_set_dev(bio, vol->dev->bdev->bdev);
		return DM_MAPIO_REMAPPED;
	}

	/* At this point, the bio has data. Do a few sanity checks */
	/* TODO: I think we can get rid of all of them */

        /* Check that it is properly aligned and it doesn't cross vector boundaries */
        if (unlikely(!sflegc_bio_isAligned(bio))) {
                pr_err("Unaligned bio!\n");
                return DM_MAPIO_KILL;
        }
        /* If it contains more than one SFLC sector, complain with the DM layer and continue */
        if (unlikely(bio->bi_iter.bi_size > SFLEGC_DEV_SECTOR_SIZE)) {
		pr_notice("Large bio of size %u\n", bio->bi_iter.bi_size);
                dm_accept_partial_bio(bio, SFLEGC_DEV_SECTOR_SCALE);
        }
	/* Check that it contains exactly one SFLC sector */
        if (unlikely(bio->bi_iter.bi_size != SFLEGC_DEV_SECTOR_SIZE)) {
                pr_err("Wrong length (%u) of bio\n", bio->bi_iter.bi_size);
                return DM_MAPIO_KILL;
        }

	/* Now it is safe, process it */
	err = sflegc_vol_processBio(vol, bio);
        if (err) {
                pr_err("Could not enqueue bio\n");
                return DM_MAPIO_KILL;
        }

	return DM_MAPIO_SUBMITTED;
}

/* Callback executed to inform the DM about our 4096-byte sector size */
static void sflegc_tgt_ioHints(struct dm_target *ti, struct queue_limits *limits)
{
	limits->logical_block_size = SFLEGC_DEV_SECTOR_SIZE;
	limits->physical_block_size = SFLEGC_DEV_SECTOR_SIZE;

	limits->io_min = SFLEGC_DEV_SECTOR_SIZE;
	limits->io_opt = SFLEGC_DEV_SECTOR_SIZE;

	return;
}

/* Callback needed for God knows what, otherwise io_hints never gets called */
static int sflegc_tgt_iterateDevices(struct dm_target *ti, iterate_devices_callout_fn fn,
									void *data)
{
	struct sflc_volume_base *sv_base = ti->private;
	sflegc_Volume *vol = container_of(sv_base, sflegc_Volume, sv_base);
	sflegc_Device * dev = vol->dev;


	if (!fn) {
		return -EINVAL;
	}
	return fn(ti, vol->dev->bdev, 0, 
		(dev->dev_header_size + dev->tot_slices * SFLEGC_DEV_PHYS_SLICE_SIZE) * SFLEGC_DEV_SECTOR_SCALE,
		data);
}
