/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*  
 * A device represents the underlying "real" block device, common to all
 * "virtual" volumes that map onto it. It also groups other structures
 * shared between volumes on the same device.
 * Devices are kept in a list. Volumes mapping onto a device are kept
 * in an array within the device, because their index is important (they
 * are stored in increasing degree of "secrecy").
 */

#ifndef _SFLEGC_DEVICE_DEVICE_H_
#define _SFLEGC_DEVICE_DEVICE_H_


/*****************************************************
 *             TYPES FORWARD DECLARATIONS            *
 *****************************************************/

/* Necessary since device.h, volume.h, and sysfs.h all include each other */

typedef struct sflegc_device_s sflegc_Device;
typedef struct sflegc_dev_iv_cache_entry_s sflegc_dev_IvCacheEntry;


/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/device-mapper.h>

#include "sflc_types.h"
#include "legacy/sflc_legacy.h"
#include "legacy/volume/volume.h"
#include "legacy/crypto/symkey/symkey.h"


/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/* We need 4096-byte sectors to amortise the space overhead of the IVs */
#define SFLEGC_DEV_SECTOR_SIZE 4096
/* A SFLC sector encompasses 8 kernel sectors */
#define SFLEGC_DEV_SECTOR_SCALE (SFLEGC_DEV_SECTOR_SIZE / SECTOR_SIZE)
/* An IV block holds IVs for 256 data blocks */
#define SFLEGC_DEV_SECTOR_TO_IV_RATIO (SFLEGC_DEV_SECTOR_SIZE / SFLEGC_SK_IV_LEN)

/* Max number of volumes linked to a single device */
#define SFLEGC_DEV_MAX_VOLUMES 15

/* A physical slice contains the 256 encrypted data blocks and the IV block */
#define SFLEGC_DEV_PHYS_SLICE_SIZE (SFLEGC_VOL_LOG_SLICE_SIZE + (SFLEGC_VOL_LOG_SLICE_SIZE / SFLEGC_DEV_SECTOR_TO_IV_RATIO))

/* Value marking a PSI as unassigned */
#define SFLEGC_DEV_RMAP_INVALID_VOL 0xFFU

/* Maximum number of open devices in total across shufflecake */
#define SFLEGC_DEV_MAX_DEVICES_TOT 1024


/*****************************************************
 *                       TYPES                       *
 *****************************************************/

struct sflegc_dev_iv_cache_entry_s
{
	/* The PSI it refers to */
	u32			psi;
	/* The actual data, containing the 4-kB IV block */
	struct page	      * iv_page;

	/* How many processes are holding it (can only be flushed when it's 0) */
	u16			refcnt;
	/* How many changes have been performed since the last flush */
	u16			dirtyness;

	/* Position in the LRU list */
	struct list_head	lru_node;
};

struct sflegc_device_s
{
	/* Base device object */
	struct sflc_device_base		sd_base;

	/* Underlying block device */
	struct dm_dev                 * bdev;
	/* Target instance that owns the bdev reference */
	struct dm_target		*ti;

	/* All volumes linked to this device */
	sflegc_Volume                    * vol[SFLEGC_DEV_MAX_VOLUMES];
	int                             vol_cnt;

	/* Reverse slice map, associating PSIs to volume indices */
	u8			      	* rmap;
	/* Shuffled array of PSIs, with advancement counter */
	u32			        *shuffled_psi_array;
	u32				shuffled_psi_ctr;
	/* Lock for all three of these objects */
	struct mutex			slices_lock;

	/* Slices info */
	u32				tot_slices;
	u32				free_slices;

	/* Header info */
	u32				vol_header_nr_iv_blocks;
	u32				vol_header_size;
	u32				dev_header_size;

	/* LRU cache of IV blocks */
	struct mutex			iv_cache_lock;
	wait_queue_head_t		iv_cache_waitqueue;
	sflegc_dev_IvCacheEntry	     ** iv_cache;
	u32				iv_cache_nr_entries;
	struct list_head		iv_lru_list;
};


/*****************************************************
 *            PUBLIC FUNCTIONS PROTOTYPES            *
 *****************************************************/


struct sflc_device_base *sflegc_dev_ctr(struct dm_target *ti, int argc, char **argv);
void sflegc_dev_dtr(struct sflc_device_base *sd_base);

/* Returns false if volume index was already occupied. */
bool sflegc_dev_addVolume(sflegc_Device * dev, sflegc_Volume * vol, int vol_idx);

/* Does not put the volume. Returns false if was already NULL. */
bool sflegc_dev_removeVolume(sflegc_Device * dev, int vol_idx);


/* Synchronously reads/writes one 4096-byte sector from/to the underlying device 
   to/from the provided page */
int sflegc_dev_rwSector(sflegc_Device * dev, struct page * page, sector_t sector, int rw);


/* The caller needs to hold slices_lock to call these functions */

/* Sets the PSI as owned by the given volume (also decreases free_slices).
 * Returns < 0 if already taken. */
int sflegc_dev_markPsiTaken(sflegc_Device * dev, u32 psi, u8 vol_idx);

/* Returns a random free physical slice, or < 0 if error */
s32 sflegc_dev_getNextRandomFreePsi(sflegc_Device * dev);


/* These functions provide concurrent-safe access to the entries of the IV cache.
   The lock iv_cache_lock is acquired by these functions: it must not be held by the caller.
   The individual entries (IV blocks) are not concurrent-safe because they don't need to:
   concurrent consumers of the same block are concerned with different IVs contained in the
   block, because we exclued concurrent I/O to the same logical data block (BIG QUESTION MARK HERE).
   When the refcount reaches 0, the IV block is flushed. */

/* Get a pointer to the specified IV block. Increases the refcount and possibly the dirtyness (if WRITE). */
u8 * sflegc_dev_getIvBlockRef(sflegc_Device * dev, u32 psi, int rw);

/* Signal end of usage of an IV block. Decreases the refcount. */
int sflegc_dev_putIvBlockRef(sflegc_Device * dev, u32 psi);

/* Flush all dirty IV blocks */
void sflegc_dev_flushIvs(sflegc_Device * dev);


#endif /* _SFLEGC_DEVICE_DEVICE_H_ */
