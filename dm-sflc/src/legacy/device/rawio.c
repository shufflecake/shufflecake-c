/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "legacy/device/device.h"
#include "legacy/utils/pools.h"
#include "legacy/log/log.h"

#include <linux/bio.h>
#include <linux/errno.h>

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/*****************************************************
 *           PRIVATE FUNCTIONS PROTOTYPES            *
 *****************************************************/

/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/* Synchronously reads/writes one 4096-byte sector from/to the underlying device 
   to/from the provided page */
int sflegc_dev_rwSector(sflegc_Device * dev, struct page * page, sector_t sector, int rw)
{
        struct bio *bio;
        blk_opf_t opf;
        int err;

        /* Synchronous READ/WRITE */
        opf = ((rw == READ) ? REQ_OP_READ : REQ_OP_WRITE);
        opf |= REQ_SYNC;

        /* Allocate bio */
        bio = bio_alloc_bioset(dev->bdev->bdev, 1, opf,  GFP_NOIO, &sflegc_pools_bioset);
        if (!bio) {
                pr_err("Could not allocate bio\n");
                return -ENOMEM;
        }

        /* Set sector */
        bio->bi_iter.bi_sector = sector * SFLEGC_DEV_SECTOR_SCALE;
        /* Add page */
        if (!bio_add_page(bio, page, SFLEGC_DEV_SECTOR_SIZE, 0)) {
                pr_err("Catastrophe: could not add page to bio! WTF?\n");
                err = EINVAL;
                goto out;
        }

        /* Submit */
        err = submit_bio_wait(bio);

out:
        /* Free and return; */
        bio_put(bio);
        return err;
}

/*****************************************************
 *          PRIVATE FUNCTIONS DEFINITIONS            *
 *****************************************************/
