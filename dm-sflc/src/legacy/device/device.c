/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * This file only implements the device-related device management functions.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "legacy/sflc_legacy.h"
#include "legacy/device/device.h"
#include "legacy/utils/vector.h"
#include "legacy/log/log.h"
#include "sflc.h"

#include <linux/vmalloc.h>

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/*****************************************************
 *           PUBLIC VARIABLES DEFINITIONS            *
 *****************************************************/


/* Initialises and pre-shuffles the PSI array */
static int sflegc_dev_initAndShufflePsiArray(u32 *psi_array, u32 len);


/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

/**
 * Creates Device. Returns an ERR_PTR() if unsuccessful.
 * Arguments:
 * 	argv[0]: Shufflecake mode: legacy/lite
 * 	argv[1]: Shufflecake-unique device ID
 * 	argv[2]: path to underlying physical device
 * 	argv[3]: volume index within the device
 * 	argv[4]: number of 1 MB slices in the underlying device
 *	argv[5]: 32-byte encryption key (hex-encoded)
 */
struct sflc_device_base *sflegc_dev_ctr(struct dm_target *ti, int argc, char **argv)
{
	sflegc_Device * dev;
	u32 tot_slices;
	int err;
	int i;

	/* Allocate device */
	dev = kzalloc(sizeof(sflegc_Device), GFP_KERNEL);
	if (!dev) {
		pr_err("Could not allocate %lu bytes for sflegc_Device\n", sizeof(sflegc_Device));
		err = -ENOMEM;
		goto err_alloc_dev;
	}

	/* Init base part */
	err = sflc_dev_base_init(&dev->sd_base, ti, argc, argv);
	if (err)
		goto bad_base;
	dev->sd_base.mode = SFLC_MODE_LEGACY;
	dev->sd_base.ops = &sflc_legacy_ops;

	/* Parse args */
	if (argc != 6) {
		pr_err("Wrong argument count");
		err = -EINVAL;
		goto err_parse;
	}
	if (sscanf(argv[4], "%u", &tot_slices) != 1) {
		pr_err("Could not decode tot_slices\n");
		err = -EINVAL;
		goto err_parse;
	}

	/* Set backing real device */
	err = dm_get_device(ti, argv[2], dm_table_get_mode(ti->table), &dev->bdev);
	if (err) {
		pr_err("Could not dm_get_device: error %d\n", err);
		goto err_dm_get_dev;
	}
	dev->ti = ti;

	/* Init volumes */
	for (i = 0; i < SFLEGC_DEV_MAX_VOLUMES; ++i) {
		dev->vol[i] = NULL;
	}
	dev->vol_cnt = 0;

	/* Set slices info */
	dev->tot_slices = tot_slices;
	dev->free_slices = tot_slices;
	/* Compute header info (like in userland tool) */
	u32 nr_pmbs_per_vol = DIV_ROUND_UP(tot_slices, SFLEGC_VOL_HEADER_MAPPINGS_PER_BLOCK);
	dev->vol_header_nr_iv_blocks = DIV_ROUND_UP(nr_pmbs_per_vol, SFLEGC_VOL_LOG_SLICE_SIZE);
	dev->vol_header_size = 1 + nr_pmbs_per_vol + dev->vol_header_nr_iv_blocks;
	dev->dev_header_size = 1 + (SFLEGC_DEV_MAX_VOLUMES * dev->vol_header_size);

	/* Init slices lock */
	mutex_init(&dev->slices_lock);
	/* Allocate reverse slice map */
	dev->rmap = vmalloc(dev->tot_slices * sizeof(u8));
	if (!dev->rmap) {
		pr_err("Could not allocate reverse slice map\n");
		err = -ENOMEM;
		goto err_alloc_rmap;
	}
	/* Initialise it */
	memset(dev->rmap, SFLEGC_DEV_RMAP_INVALID_VOL, dev->tot_slices * sizeof(u8));
	/* Allocate PSI array */
	dev->shuffled_psi_array = vmalloc(dev->tot_slices * sizeof(u32));
	if (!dev->shuffled_psi_array) {
		pr_err("Could not allocate PSI array\n");
		err = -ENOMEM;
		goto err_alloc_psi_array;
	}
	/* Initialise it and pre-shuffle it */
	err = sflegc_dev_initAndShufflePsiArray(dev->shuffled_psi_array, dev->tot_slices);
	if (err) {
		pr_err("Could not init-and-shuffle PSI array: error %d", err);
		goto err_initshuffle_psi_array;
	}
	/* Init related counter */
	dev->shuffled_psi_ctr = 0;

	/* Init IV cache lock */
	mutex_init(&dev->iv_cache_lock);
	/* Init IV cache waitqueue */
	init_waitqueue_head(&dev->iv_cache_waitqueue);
	/* Allocate IV cache */
	dev->iv_cache = kzalloc(dev->tot_slices * sizeof(sflegc_dev_IvCacheEntry *), GFP_KERNEL);
	if (!dev->iv_cache) {
		pr_err("Could not allocate IV cache\n");
		err = -ENOMEM;
		goto err_alloc_iv_cache;
	}
	/* Set it empty */
	dev->iv_cache_nr_entries = 0;
	/* Init list head */
	INIT_LIST_HEAD(&dev->iv_lru_list);

	/* Add to sysfs */
	err = sflegc_sysfs_add_device(dev);
	if (err) {
		pr_err("Could not add device to sysfs; error %d\n", err);
		goto err_sysfs;
	}

	return &dev->sd_base;


err_sysfs:
	kfree(dev->iv_cache);
err_alloc_iv_cache:
err_initshuffle_psi_array:
	vfree(dev->shuffled_psi_array);
err_alloc_psi_array:
	vfree(dev->rmap);
err_alloc_rmap:
	dm_put_device(ti, dev->bdev);
err_dm_get_dev:
err_parse:
	sflc_dev_base_exit(&dev->sd_base);
bad_base:
	kfree(dev);
err_alloc_dev:
	return ERR_PTR(err);
}


void sflegc_dev_dtr(struct sflc_device_base *sd_base)
{
	sflegc_Device *dev = container_of(sd_base, sflegc_Device, sd_base);

	/* Check if we actually have to put this device */
	if (!dev) {
		return;
	}
	if (dev->vol_cnt > 0) {
		pr_warn("Called while still holding %d volumes\n", dev->vol_cnt);
		return;
	}

	/* Flush all IVs */
	sflegc_dev_flushIvs(dev);

	/* Sysfs */
	sflegc_sysfs_remove_device(dev);

	/* IV cache */
	kfree(dev->iv_cache);

	/* PSI array */
	vfree(dev->shuffled_psi_array);

	/* Reverse slice map */
	vfree(dev->rmap);

	/* Backing device */
	dm_put_device(dev->ti, dev->bdev);

	/* Base part */
	sflc_dev_base_exit(&dev->sd_base);

	/* Free the device itself */
	kfree(dev);

	return;
}


/* Initialises and pre-shuffles the PSI array */
static int sflegc_dev_initAndShufflePsiArray(u32 *psi_array, u32 len)
{
	u32 i;

	/* Init to the identity map */
	for (i = 0; i < len; i++) {
		psi_array[i] = i;
	}

	/* Permute */
	return sflegc_vec_u32shuffle(psi_array, len);
}

