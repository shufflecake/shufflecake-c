/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include <linux/module.h>

#include "legacy/sflc_legacy.h"
#include "legacy/log/log.h"

// Only to import the definitions of structs sflc_volume and sflc_device
#include "sflc.h"


/*
 *----------------------------
 * Devices
 *----------------------------
 */

/* Show the total number of slices in a device */
static ssize_t tot_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);
	sflegc_Device * dev = container_of(sd_base, sflegc_Device, sd_base);
	ssize_t ret;

	/* Write the tot_slices */
	ret = sysfs_emit(buf, "%u\n", dev->tot_slices);

	return ret;
}

/* Show the number of free slices in a device */
static ssize_t free_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_device_base *sd_base = container_of(kobj, struct sflc_device_base, kobj);
	sflegc_Device * dev = container_of(sd_base, sflegc_Device, sd_base);
	ssize_t ret;

	/* Write the free_slices */
	if (mutex_lock_interruptible(&dev->slices_lock))
		return -ERESTARTSYS;
	ret = sysfs_emit(buf, "%u\n", dev->free_slices);
	mutex_unlock(&dev->slices_lock);

	return ret;
}

static struct kobj_attribute tot_slices_kattr = __ATTR_RO(tot_slices);
static struct kobj_attribute free_slices_kattr = __ATTR_RO(free_slices);
static struct attribute *sflegc_device_attrs[] = {
	&tot_slices_kattr.attr,
	&free_slices_kattr.attr,
	NULL
};
static const struct attribute_group sflegc_device_attr_group = {
	.attrs = sflegc_device_attrs,
};

int sflegc_sysfs_add_device(sflegc_Device *dev)
{
	return sysfs_create_group(&dev->sd_base.kobj, &sflegc_device_attr_group);
}

void sflegc_sysfs_remove_device(sflegc_Device *dev)
{
	sysfs_remove_group(&dev->sd_base.kobj, &sflegc_device_attr_group);
}


/*
 *----------------------------
 * Volumes
 *----------------------------
 */

/* Show the number of mapped slices in a volume */
static ssize_t mapped_slices_show(struct kobject *kobj, struct kobj_attribute *kattr, char *buf)
{
	struct sflc_volume_base *sv_base = container_of(kobj, struct sflc_volume_base, kobj);
	sflegc_Volume * vol = container_of(sv_base, sflegc_Volume, sv_base);
	ssize_t ret;

	/* Write the free_slices */
	if (mutex_lock_interruptible(&vol->fmap_lock))
		return -ERESTARTSYS;
	ret = sysfs_emit(buf, "%u\n", vol->mapped_slices);
	mutex_unlock(&vol->fmap_lock);

	return ret;
}

static struct kobj_attribute mapped_slices_kattr = __ATTR_RO(mapped_slices);
static struct attribute *sflegc_volume_attrs[] = {
	&mapped_slices_kattr.attr,
	NULL
};
static const struct attribute_group sflegc_volume_attr_group = {
	.attrs = sflegc_volume_attrs,
};

int sflegc_sysfs_add_volume(sflegc_Volume *vol)
{
	return sysfs_create_group(&vol->sv_base.kobj, &sflegc_volume_attr_group);
}

void sflegc_sysfs_remove_volume(sflegc_Volume *vol)
{
	sysfs_remove_group(&vol->sv_base.kobj, &sflegc_volume_attr_group);
}
