/*
 *  Copyright The Shufflecake Project Authors (2022)
 *  Copyright The Shufflecake Project Contributors (2022)
 *  Copyright Contributors to the The Shufflecake Project.
 *  
 *  See the AUTHORS file at the top-level directory of this distribution and at
 *  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 *  
 *  This file is part of the program shufflecake-c, which is part of the
 *  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 *  layer for Linux. See <https://www.shufflecake.net>.
 *  
 *  This program is free software: you can redistribute it and/or modify it 
 *  under the terms of the GNU General Public License as published by the Free 
 *  Software Foundation, either version 2 of the License, or (at your option) 
 *  any later version. This program is distributed in the hope that it will be 
 *  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 *  Public License for more details. You should have received a copy of the 
 *  GNU General Public License along with this program. 
 *  If not, see <https://www.gnu.org/licenses/>.
 */
 
/* 
 * Implementations for all the bio utility functions
 */

/*****************************************************
 *                  INCLUDE SECTION                  *
 *****************************************************/

#include "legacy/utils/pools.h"
#include "legacy/log/log.h"

/*****************************************************
 *                     CONSTANTS                     *
 *****************************************************/

/* Pool sizes */
#define SFLEGC_POOLS_BIOSET_POOL_SIZE 1024
#define SFLEGC_POOLS_PAGE_POOL_SIZE 1024
#define SFLEGC_POOLS_WRITE_WORK_POOL_SIZE 1024
#define SFLEGC_POOLS_DECRYPT_WORK_POOL_SIZE 1024

/* Slab cache names */
#define SFLEGC_POOLS_WRITE_WORK_SLAB_NAME "sflegc_write_work_slab"
#define SFLEGC_POOLS_DECRYPT_WORK_SLAB_NAME "sflegc_decrypt_work_slab"
#define SFLEGC_POOLS_IV_SLAB_NAME "sflegc_iv_slab"

/*****************************************************
 *           PUBLIC VARIABLES DEFINITIONS            *
 *****************************************************/

struct bio_set sflegc_pools_bioset;
mempool_t * sflegc_pools_pagePool;
mempool_t * sflegc_pools_writeWorkPool;
mempool_t * sflegc_pools_decryptWorkPool;
struct kmem_cache * sflegc_pools_ivSlab;

/*****************************************************
 *                 PRIVATE VARIABLES                 *
 *****************************************************/

static struct kmem_cache * sflegc_pools_writeWorkSlab;
static struct kmem_cache * sflegc_pools_decryptWorkSlab;

/*****************************************************
 *           PUBLIC FUNCTIONS DEFINITIONS            *
 *****************************************************/

int sflegc_pools_init(void)
{
	int err;

	/* Memory pools: bioset */
	err = bioset_init(&sflegc_pools_bioset, SFLEGC_POOLS_BIOSET_POOL_SIZE, 0, BIOSET_NEED_BVECS);
	if (err) {
		pr_err("Could not init bioset: error %d\n", err);
		goto err_bioset;
	}

	/* Memory pools: page_pool */
	sflegc_pools_pagePool = mempool_create_page_pool(SFLEGC_POOLS_PAGE_POOL_SIZE, 0);
	if (!sflegc_pools_pagePool) {
		pr_err("Could not create page pool\n");
		err = -ENOMEM;
                goto err_pagepool;
	}

        /* Memory pools: writeWork slab cache */
        sflegc_pools_writeWorkSlab = kmem_cache_create(SFLEGC_POOLS_WRITE_WORK_SLAB_NAME, sizeof(sflegc_vol_WriteWork), 0, SLAB_POISON | SLAB_RED_ZONE, NULL);
        if (IS_ERR(sflegc_pools_writeWorkSlab)) {
                err = PTR_ERR(sflegc_pools_writeWorkSlab);
                pr_err("Could not create writeWork slab cache; error %d\n", err);
                goto err_create_write_work_slab;
        }

        /* Memory pools: writeWork pool */
        sflegc_pools_writeWorkPool = mempool_create_slab_pool(SFLEGC_POOLS_WRITE_WORK_POOL_SIZE, sflegc_pools_writeWorkSlab);
        if (!sflegc_pools_writeWorkPool) {
                pr_err("Could not create writeWork pool\n");
                err = -ENOMEM;
                goto err_write_work_pool;
        }

        /* Memory pools: decryptWork slab cache */
        sflegc_pools_decryptWorkSlab = kmem_cache_create(SFLEGC_POOLS_DECRYPT_WORK_SLAB_NAME, sizeof(sflegc_vol_DecryptWork), 0, SLAB_POISON | SLAB_RED_ZONE, NULL);
        if (IS_ERR(sflegc_pools_decryptWorkSlab)) {
                err = PTR_ERR(sflegc_pools_decryptWorkSlab);
                pr_err("Could not create decryptWork slab cache; error %d\n", err);
                goto err_create_decrypt_work_slab;
        }

        /* Memory pools: decryptWork pool */
        sflegc_pools_decryptWorkPool = mempool_create_slab_pool(SFLEGC_POOLS_DECRYPT_WORK_POOL_SIZE, sflegc_pools_decryptWorkSlab);
        if (!sflegc_pools_decryptWorkPool) {
                pr_err("Could not create decryptWork pool\n");
                err = -ENOMEM;
                goto err_decrypt_work_pool;
        }

	/* Memory pools: IV slab cache */
        sflegc_pools_ivSlab = kmem_cache_create(SFLEGC_POOLS_IV_SLAB_NAME, sizeof(sflegc_dev_IvCacheEntry), 0, SLAB_POISON | SLAB_RED_ZONE, NULL);
        if (IS_ERR(sflegc_pools_ivSlab)) {
                err = PTR_ERR(sflegc_pools_ivSlab);
                pr_err("Could not create IV slab cache; error %d\n", err);
                goto err_create_iv_slab;
        }

        return 0;


err_create_iv_slab:
        mempool_destroy(sflegc_pools_decryptWorkPool);
err_decrypt_work_pool:
        kmem_cache_destroy(sflegc_pools_decryptWorkSlab);
err_create_decrypt_work_slab:
        mempool_destroy(sflegc_pools_writeWorkPool);
err_write_work_pool:
        kmem_cache_destroy(sflegc_pools_writeWorkSlab);
err_create_write_work_slab:
        mempool_destroy(sflegc_pools_pagePool);
err_pagepool:
        bioset_exit(&sflegc_pools_bioset);
err_bioset:
	return err;
}

void sflegc_pools_exit(void)
{
        kmem_cache_destroy(sflegc_pools_ivSlab);
        mempool_destroy(sflegc_pools_decryptWorkPool);
        kmem_cache_destroy(sflegc_pools_decryptWorkSlab);
        mempool_destroy(sflegc_pools_writeWorkPool);
        kmem_cache_destroy(sflegc_pools_writeWorkSlab);
        mempool_destroy(sflegc_pools_pagePool);
        bioset_exit(&sflegc_pools_bioset);
}
