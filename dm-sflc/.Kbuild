 #
 #  Copyright The Shufflecake Project Authors (2022)
 #  Copyright The Shufflecake Project Contributors (2022)
 #  Copyright Contributors to the The Shufflecake Project.
 #  
 #  See the AUTHORS file at the top-level directory of this distribution and at
 #  <https://www.shufflecake.net/permalinks/shufflecake-userland/AUTHORS>
 #  
 #  This file is part of the program shufflecake-c, which is part of the
 #  Shufflecake Project. Shufflecake is a plausible deniability (hidden storage)
 #  layer for Linux. See <https://www.shufflecake.net>.
 #  
 #  This program is free software: you can redistribute it and/or modify it 
 #  under the terms of the GNU General Public License as published by the Free 
 #  Software Foundation, either version 2 of the License, or (at your option) 
 #  any later version. This program is distributed in the hope that it will be 
 #  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
 #  Public License for more details. You should have received a copy of the 
 #  GNU General Public License along with this program. 
 #  If not, see <https://www.gnu.org/licenses/>.
 #
 
MODULE_NAME := dm_sflc
obj-m := $(MODULE_NAME).o


OBJ_LIST := sflc.o dev_vol.o sysfs.o

OBJ_LIST += legacy/sflc_legacy.o legacy/target.o legacy/sysfs.o
OBJ_LIST += legacy/device/device.o legacy/device/volumes.o legacy/device/rawio.o legacy/device/rmap.o legacy/device/iv.o
OBJ_LIST += legacy/volume/volume.o legacy/volume/io.o legacy/volume/read.o legacy/volume/write.o legacy/volume/fmap.o
OBJ_LIST += legacy/utils/string.o legacy/utils/bio.o legacy/utils/pools.o legacy/utils/workqueues.o legacy/utils/vector.o
OBJ_LIST += legacy/crypto/rand/rand.o
OBJ_LIST += legacy/crypto/symkey/symkey.o legacy/crypto/symkey/skreq_pool.o

OBJ_LIST += lite/sflc_lite.o lite/sysfs.o
OBJ_LIST += lite/device.o lite/volume.o 
OBJ_LIST += lite/posmap.o lite/read.o lite/write.o lite/crypto.o

$(MODULE_NAME)-y += $(OBJ_LIST)


# Normal CC flags
ccflags-y := -O2 
ccflags-y += -I$(src)
ccflags-y += -Wall -Wno-declaration-after-statement

# Debug CC flags
ccflags-$(CONFIG_SFLC_DEBUG) += -DDEBUG
ccflags-$(CONFIG_SFLC_DEBUG) += -Og -g
ccflags-$(CONFIG_SFLC_DEBUG) += -fsanitize=kernel-address -fno-omit-frame-pointer

