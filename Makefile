#  Copyright The Shufflecake Project Authors (2022)
#  Copyright The Shufflecake Project Contributors (2022)
#  Copyright Contributors to the The Shufflecake Project.
  
#  See the AUTHORS file at the top-level directory of this distribution and at
#  <https://www.shufflecake.net/permalinks/shufflecake-c/AUTHORS>
  
#  This file is part of the program shufflecake-c, which is part of the Shufflecake 
#  Project. Shufflecake is a plausible deniability (hidden storage) layer for 
#  Linux. See <https://www.shufflecake.net>.
  
#  This program is free software: you can redistribute it and/or modify it 
#  under the terms of the GNU General Public License as published by the Free 
#  Software Foundation, either version 2 of the License, or (at your option) 
#  any later version. This program is distributed in the hope that it will be 
#  useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General 
#  Public License for more details. You should have received a copy of the 
#  GNU General Public License along with this program. 
#  If not, see <https://www.gnu.org/licenses/>.


# Just a passthrough Makefile for both kernel module and userland tool

default:
	make -C dm-sflc
	cp dm-sflc/bin/dm-sflc.ko ./dm-sflc.ko
	make -C shufflecake-userland
	cp shufflecake-userland/bin/proj_build/shufflecake ./shufflecake

debug:
	make -C dm-sflc debug
	cp dm-sflc/bin/dm-sflc.ko ./dm-sflc.ko
	make -C shufflecake-userland debug
	cp shufflecake-userland/bin/proj_build/shufflecake ./shufflecake

test:
	make -C shufflecake-userland test
	
install:
	# Just a placeholder for installation, for now equivalent to `make`
	make -C dm-sflc install
	cp dm-sflc/bin/dm-sflc.ko ./dm-sflc.ko
	make -C shufflecake-userland install
	cp shufflecake-userland/bin/proj_build/shufflecake ./shufflecake

clean:
	make -C dm-sflc clean
	make -C shufflecake-userland clean
	rm ./shufflecake
	rm ./dm-sflc.ko
	
	
	
	
	
